import unittest
import socket
import asyncio
from typing import List

from testutils import *
from components.channel import Channel, ProtocolError, ChannelClosedError

import struct
from components.channel import HEADER_FORMAT, HEADER_SIGNATURE

class TestChannel(unittest.IsolatedAsyncioTestCase):

    async def asyncSetUp(self) -> None:
        self.sockA, self.sockB = socket.socketpair()
        self.readerA, self.writerA = await asyncio.open_connection(sock=self.sockA)
        self.readerB, self.writerB = await asyncio.open_connection(sock=self.sockB)

    async def asyncTearDown(self) -> None:
        async def close(writer: asyncio.StreamWriter):
            if not writer.is_closing(): writer.close()
            await writer.wait_closed()
        await asyncio.gather(close(self.writerA), close(self.writerB), return_exceptions=True)


    async def test_close(self):
        channel = Channel(self.readerA, self.writerA)
        close_task = asyncio.create_task(channel.close())
        await asyncio.sleep(0)
        self.assertTrue(channel.is_closing)
        self.assertFalse(channel.is_closed)
        await close_task
        self.assertTrue(channel.is_closing)
        self.assertTrue(channel.is_closed)
        self.assertTrue(self.writerA.is_closing())

    async def test_close_is_idempotent(self):
        channel = Channel(self.readerA, self.writerA)
        task1 = asyncio.create_task(channel.close())
        task2 = asyncio.create_task(channel.close())
        await asyncio.gather(task1, task2)
        self.assertTrue(self.writerA.is_closing())

    async def test_close_when_closed(self):
        channel = Channel(self.readerA, self.writerA)
        await channel.close()
        self.assertTrue(channel.is_closed)
        await channel.close()
        self.assertTrue(channel.is_closed)

    async def test_recv_send(self):
        receiver = Channel(self.readerA, self.writerA)
        sender = Channel(self.readerB, self.writerB)
        msg = b'test'
        await sender.send(msg)
        got = await receiver.recv()
        self.assertEqual(msg, got)

    async def test_recv_when_closed(self):
        receiver = Channel(self.readerA, self.writerA)
        await receiver.close()
        with self.assertRaises(ChannelClosedError):
            await receiver.recv()

    async def test_recv_when_remote_closed(self):
        receiver = Channel(self.readerA, self.writerA)
        sender = Channel(self.readerB, self.writerB)
        await sender.close()
        with self.assertRaises(ChannelClosedError):
            await receiver.recv()

    async def test_recv_invalid_header(self):
        receiver = Channel(self.readerA, self.writerA)
        with self.assertRaises(ProtocolError):
            self.sockB.send(b'aaaaaaaaa')
            await receiver.recv()
        self.assertTrue(receiver.is_closed)

    async def test_recv_incomplete(self):
        
        async def delay_close(ch: Channel, time):
            await asyncio.sleep(time)
            await ch.close()

        receiver = Channel(self.readerA, self.writerA)
        sender = Channel(self.readerB, self.writerB)
        with self.assertRaises(ChannelClosedError):
            header = struct.pack(HEADER_FORMAT, HEADER_SIGNATURE, 20)
            self.sockB.send(header)
            self.sockB.send(b'abc')
            await asyncio.gather(
                delay_close(sender, 50),
                receiver.recv(message_timeout=0.1)
            )
        self.assertTrue(receiver.is_closed)

    async def test_send_when_closed(self):
        sender = Channel(self.readerA, self.writerA)
        await sender.close()
        with self.assertRaises(ChannelClosedError):
            await sender.send(b'test')

    @unittest.skip('Does not raise')
    async def test_send_when_remote_closed(self):
        sender = Channel(self.readerA, self.writerA)
        receiver = Channel(self.readerB, self.writerB)
        await receiver.close()
        with self.assertRaises(ChannelClosedError):
            await sender.send(b'test')


    async def test_as_iter(self):

        async def receive(ch: Channel) -> List[bytes]:
            received = []
            async for msg in ch:
                received.append(msg)
            return received
        
        async def send(ch: Channel, msgs: List[bytes]):
            for msg in msgs: await ch.send(msg)
            await asyncio.sleep(0.05)
            await ch.close()

        msgs = [b'test1', b'test2', b'test3']
        sender = Channel(self.readerA, self.writerA)
        receiver = Channel(self.readerB, self.writerB)

        _, received = await asyncio.gather(
            send(sender, msgs),
            receive(receiver)
        )
        self.assertListEqual(msgs, received)


    async def test_as_context_manager(self):
        channel = Channel(self.readerA, self.writerA)
        async with channel:
            await asyncio.sleep(0)
        self.assertTrue(channel.is_closed)


if __name__ == "__main__":
    unittest.main()
