import asyncio
import logging
import testutils
from service.video_capture import VideoSourceOpener

logging.basicConfig(level=0, format="%(asctime)s %(levelname)-7s %(name)s - %(message)s")

async def test():
    opener = VideoSourceOpener(logging.getLogger('test'))
    open_ready = asyncio.Event()

    opener_id = 0
    def next_id():
        nonlocal opener_id
        opener_id += 1
        return opener_id

    async def open(path, width=640, height=480):
        log = logging.getLogger(f'task#{next_id()}')
        log.info(f'opening {path}')
        try:
            task = asyncio.create_task(
                opener.open(path, width, height)
            )
            await asyncio.sleep(0)
            open_ready.set()
            container, source_id = await task
            log.info(f'opened {container} with id {source_id}')
            if container: container.close()
            log.info(f'closed {container}')
        except asyncio.CancelledError:
            log.info('cancelled')
        except Exception as err:
            log.error(f'error: {err!r}')
        finally:
            open_ready.clear()

    print('--- Test uninterrupted opening ---')
    await open('/dev/video0')

    print('--- Test opening a non-compatible device ---')
    await open('/dev/video2')

    print('--- Test opening a bad file ---')
    await open('/dev/video87')

    print('--- Test interrupted opening ---')
    task = asyncio.create_task(open('/dev/video4'))
    await open_ready.wait()
    task.cancel()
    # make sure the opener has finished
    await opener.join()
    if len(opener.opener_tasks) != 0:
        print('ERROR: not all tasks have stopped')

    print('--- Test interrupted opening that fails ---')
    task = asyncio.create_task(open('/dev/video87'))
    await open_ready.wait()
    task.cancel()
    # make sure the opener has finished
    await opener.join()
    if len(opener.opener_tasks) != 0:
        print('ERROR: not all tasks have stopped')

    print('--- Test interrupted opening that fails ---')
    task = asyncio.create_task(open('/dev/video2'))
    await open_ready.wait()
    task.cancel()
    # make sure the opener has finished
    await opener.join()
    if len(opener.opener_tasks) != 0:
        print('ERROR: not all tasks have stopped')

    # now let open all three at the same time
    print('--- Test multiple openers ---')
    await asyncio.gather(
        open('/dev/video0', 640, 480),
        open('/dev/video2', 640, 360),
        open('/dev/video4', 640, 480),
        return_exceptions=True
    )

if __name__ == "__main__":
    asyncio.run(test())

