import asyncio
import unittest
from pyee import EventEmitter

import testutils
from components.session import Session


class MockChannel(EventEmitter):
    def __init__(self):
        super().__init__()
        self.is_closed = False
    async def close(self):
        self.is_closed = True


class TestSession(unittest.TestCase):
    
    def test_init(self):
        session = Session(MockChannel(), a=42, b='test')
        self.assertIn('a', session)
        self.assertIn('b', session)
        self.assertNotIn('c', session)
        self.assertEqual(session['a'], 42)
        self.assertEqual(session['b'], 'test')
    
    def test_close(self):
        channel = MockChannel()
        session = Session(channel)
        asyncio.run(session.close())
        self.assertTrue(channel.is_closed)

    def test_close_twice(self):
        channel = MockChannel()
        session = Session(channel)
        loop = asyncio.new_event_loop()
        loop.run_until_complete(session.close())
        loop.run_until_complete(session.close())
        self.assertTrue(channel.is_closed)

    def test_get_set_value(self):
        session = Session(MockChannel())
        session['a'] = 42
        session['b'] = 'test'
        self.assertIn('a', session)
        self.assertIn('b', session)
        self.assertEqual(session['a'], 42)
        self.assertEqual(session['b'], 'test')
        self.assertIsNone(session['c'])

    def test_del_value(self):
        session = Session(MockChannel(), a=42, b='test')        
        del session['b']
        del session['c']
        self.assertIn('a', session)
        self.assertNotIn('b', session)
        self.assertNotIn('c', session)
        self.assertIsNone(session['b'])

    def test_contains_key(self):
        session = Session(MockChannel(), a=42)
        self.assertTrue('a' in session)
        self.assertFalse('b' in session)

if __name__ == "__main__":
    unittest.main()
