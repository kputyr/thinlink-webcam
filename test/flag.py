import sys
import asyncio
from io import BufferedWriter
import fcntl
from v4l2py import device as v4l2dev
from v4l2py import raw as v4l2

from testutils import *

from video.v4l2utils import V4L2Recorder
from utils.video import GeneratedVideoStreamTrack,\
    create_horizontal_flag, \
    animated_flag

def show(text):
    print(text, end="", flush=True)

devIndex = 0
width = 480
height = 640

def pixel_format(
    width,
    height,
    pix_fmt,
    colorspace
):
    format = v4l2.v4l2_format()
    format.type = v4l2.V4L2_BUF_TYPE_VIDEO_OUTPUT
    format.fmt.pix.pixelformat = pix_fmt
    format.fmt.pix.width = width
    format.fmt.pix.height = height
    format.fmt.pix.field = v4l2.V4L2_FIELD_NONE
    format.fmt.pix.colorspace = colorspace
    return format

def RGB24(width, height):
    format = pixel_format(
        width,
        height,
        pix_fmt=v4l2.V4L2_PIX_FMT_RGB24,
        colorspace=v4l2.V4L2_COLORSPACE_SRGB
    )
    format.fmt.pix.bytesperline = width * 3
    format.fmt.pix.sizeimage = width * height * 3
    return format

def YUV422(width, height):
    format = pixel_format(
        width,
        height,
        pix_fmt = v4l2.V4L2_PIX_FMT_YUYV,
        colorspace = v4l2.V4L2_COLORSPACE_JPEG
    )
    format.fmt.pix.bytesperline = width * 2
    format.fmt.pix.sizeimage = width * height * 2
    return format

def YUV420(width, height):
    format = pixel_format(
        width,
        height,
        pix_fmt = v4l2.V4L2_PIX_FMT_YUV420,
        colorspace = v4l2.V4L2_COLORSPACE_JPEG
    )
    bytesperline = width + (width >> 1)
    format.fmt.pix.bytesperline = bytesperline
    format.fmt.pix.sizeimage = bytesperline * height
    return format


formats = {
    'rgb24': RGB24,      # fine for Chrome
    'yuv420p': YUV420,    # fine for Chrome
    'yuv422p': YUV422
}

format_name = sys.argv[1] if len(sys.argv) > 1 and sys.argv[1] in formats else 'rgb24'

show("Generating frames...")
flag = create_horizontal_flag()
video = animated_flag(flag, width, height)
print("OK")
if format != 'rgb24':
    show(f"Encoding frames into {format_name}...")
    video = [frame.reformat(format=format_name) for frame in video]
    print("OK")

for frame in video:
    print("{}x{} {} [{}]".format(
        frame.width,
        frame.height,
        frame.format.name,
        ":".join( (str(plane.buffer_size) for plane in frame.planes) )
    ))

show("OK\nOpening the device...")
devName = f"/dev/video{devIndex}"
device = v4l2dev.fopen(devName, rw=True)
caps = v4l2dev.read_info(device.fileno())
print(f"OK\nDevice info: {caps}")
print("Accessed directly:")
print(f"- physical caps: {caps.device_capabilities}")
print(f"- capabilities: {caps.capabilities}")
print(f"- driver: {caps.driver}")

show("Setting format...")
pix_fmt = formats.get(format_name)(width, height)
result = fcntl.ioctl(device, v4l2.VIDIOC_S_FMT, pix_fmt)
print(result)

async def main():
    device_buffer = BufferedWriter(device, pix_fmt.fmt.pix.sizeimage)
    track = GeneratedVideoStreamTrack(video)
    recorder = V4L2Recorder(device_buffer)
    recorder.addTrack(track)
    print("Playing the movie")
    await recorder.start()
    try:
        await asyncio.sleep(300)
    except KeyboardInterrupt:
        pass
    except Exception as err:
        print(err)
    finally:
        await recorder.stop()
        device_buffer.close()
        print("Playback has stopped")

asyncio.run(main())
device.close()
