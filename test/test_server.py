import unittest
import asyncio
from typing import Set
import os

import testutils

from components.channel import Channel
from components.session import Session, SimpleClientSession
from service.server import AbstractServer, TcpServer, UnixServer



class TestServer:

    class Base(unittest.IsolatedAsyncioTestCase):

        async def start_server(self) -> AbstractServer:
            raise NotImplementedError
        
        async def connect(self, server: AbstractServer) -> Session:
            raise NotImplementedError

        def setUp(self) -> None:
            self.servers: Set[AbstractServer] = set()
            self.clients: Set[Session] = set()
        
        async def asyncTearDown(self) -> None:
            server_tasks = (server.stop() for server in self.servers)
            client_tasks = (client.close() for client in self.clients)
            await asyncio.gather(*server_tasks, *client_tasks)

        async def test_start_stop(self):
            server = await self.start_server()
            self.assertTrue(server.is_running)
            await server.wait_serving()
            self.assertTrue(server.is_serving)
            await server.stop()
            self.assertFalse(server.is_running)
            self.assertFalse(server.is_serving)

        async def test_connected(self):
            server = await self.start_server()
            await self.connect(server)
            self.assertTrue(server.is_serving)

        async def test_disconnected(self):
            server = await self.start_server()
            client = await self.connect(server)
            await client.close()
            self.assertTrue(client.is_closed)

        async def test_requests(self):
            server = await self.start_server()
            client = await self.connect(server)

            got = await client.request(b'echo', b'test-data')
            self.assertEqual(got, b'test-data')

            await client.notify(b'vset', b'abc\nMy value')
            got = await client.request(b'vget', b'abc')
            self.assertEqual(got, b'My value')


class TestTcpServer(TestServer.Base):
    async def start_server(self) -> TcpServer:
        server = testutils.build_tcp_server()
        self.servers.add(server)
        await server.start()
        return server

    async def connect(self, server: TcpServer) -> Session:
        await server.wait_serving()
        reader, writer = await asyncio.open_connection('localhost', server.port)
        client = SimpleClientSession(Channel(reader, writer))
        self.clients.add(client)
        return client


class TestUnixServer(TestServer.Base):
    SERVER_FILE = 'test'
    FILE_INDEX: int = 0

    @classmethod
    def get_server_file(cls) -> str:
        file = cls.SERVER_FILE + str(cls.FILE_INDEX)
        cls.FILE_INDEX += 1
        return os.path.join(os.path.dirname(__file__), file)

    async def start_server(self) -> UnixServer:
        server = testutils.build_unix_server(self.get_server_file())
        self.servers.add(server)
        await server.start()
        return server

    async def connect(self, server: UnixServer) -> Session:
        await server.wait_serving()
        reader, writer = await asyncio.open_unix_connection(server.path)
        client = SimpleClientSession(Channel(reader, writer))
        self.clients.add(client)
        return client
    
    async def test_sock_file_created(self):
        server = await self.start_server()
        await server.wait_serving()
        self.assertTrue(os.path.exists(server.path))

    async def test_sock_file_removed(self):
        server = await self.start_server()
        await server.wait_serving()
        await server.stop()
        self.assertFalse(os.path.exists(server.path))


if __name__ == "__main__":
    unittest.main(verbosity=2)
