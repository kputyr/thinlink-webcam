import unittest
import testutils 
from testutils.mock_camera_info import mock_cameras

from service.video_monitor import CameraInfo, CameraCollection

class TestCameraCollection(unittest.TestCase):
    
    def setUp(self):
        self.cams = [cam[0] for cam in mock_cameras]

    def test_get_by_exact_id(self):
        cams = CameraCollection(self.cams)
        self.assertEqual(cams.get_id(self.cams[2].id), self.cams[2])
        self.assertEqual(cams.get_id(self.cams[3].id), self.cams[3])
        self.assertIsNone(cams.get_id('none'))

    def test_get_by_hash(self):
        cams = CameraCollection(self.cams)
        # cam #0 and #1 have the same hash
        self.assertEqual(cams.get_id(self.cams[1].id[:-2]), self.cams[0])
        # cam #2 and #3 have different hashes
        self.assertEqual(cams.get_id(self.cams[2].id[:-2]), self.cams[2])
        self.assertEqual(cams.get_id(self.cams[3].id[:-2]), self.cams[3])

    def test_get_by_index(self):
        cams = CameraCollection(self.cams)
        self.assertEqual(cams.get_id("1"), self.cams[0])
        self.assertEqual(cams.get_id("3"), self.cams[2])
        self.assertIsNone(cams.get_id("0"))
        self.assertIsNone(cams.get_id("x"))
        self.assertIsNone(cams.get_id(str(len(cams)+1)))

    def test_get_path(self):
        cams = CameraCollection(self.cams)
        self.assertEqual(cams.get_path(self.cams[2].path), self.cams[2])
        self.assertEqual(cams.get_path(self.cams[3].path), self.cams[3])
        self.assertIsNone(cams.get_path('none'))

    def test_remove_id(self):
        cams = CameraCollection(self.cams)
        self.assertEqual(cams.remove_id(self.cams[2].id), self.cams[2])
        self.assertIsNone(cams.get_id(self.cams[2].id))
        self.assertEqual(cams.remove_id(self.cams[3].id), self.cams[3])
        self.assertIsNone(cams.get_id(self.cams[3].id))
        self.assertIsNone(cams.remove_id('none'))
        self.assertEqual(len(cams), len(self.cams)-2)

    def test_remove_path(self):
        cams = CameraCollection(self.cams)
        self.assertEqual(cams.remove_path(self.cams[2].path), self.cams[2])
        self.assertIsNone(cams.get_id(self.cams[2].id))
        self.assertEqual(cams.remove_path(self.cams[3].path), self.cams[3])
        self.assertIsNone(cams.get_id(self.cams[3].id))
        self.assertIsNone(cams.remove_path('none'))
        self.assertEqual(len(cams), len(self.cams)-2)



if __name__ == "__main__":
    unittest.main(verbosity=2)
