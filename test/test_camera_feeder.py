import asyncio
from asyncio import subprocess as sp
import traceback
import av
from typing import Optional

import testutils

from service.video_feeder import CameraFeeder, VideoOutputDevice
from aiortc import MediaStreamTrack
from aiortc.contrib.media import MediaPlayer

TARGET_INDEX=10

class AppData:
    source_path: Optional[str] = None
    source: Optional[MediaPlayer] = None
    feeder: Optional[CameraFeeder] = None


async def wait(seconds: int):
    dots = seconds * 2
    print("".ljust(dots, "."))
    while dots > 0:
        await asyncio.sleep(0.5)
        print("`", end="", flush=True)
        dots -= 1
    print("\n", end="", flush=True)


async def set_source(feeder: CameraFeeder, path: str, wait_time: int = 0):
    try:
        if AppData.source:
            print(f"SET SOURCE: closing {AppData.source_path}")
            AppData.source.video.stop()
        if path:
            print(f"SET SOURCE: opening {path}")
            AppData.source_path = path
            AppData.source = MediaPlayer(path, format='v4l2', options={
                'video_size': '640x480',
#                'input_format': 'mjpeg'
            })
    except Exception as err:
        print(f"SET SOURCE ERROR: {err}")
        AppData.source_path = None
        AppData.source = None
        print(traceback.format_exc())
    finally:
        feeder.set_track(AppData.source and TrackDecoder(AppData.source.video))
        print(f"SET SOURCE: done")
    if wait_time: await wait(wait_time)


class TrackDecoder(MediaStreamTrack):

    kind = "video"

    def __init__(self, track: MediaStreamTrack):
        super().__init__()
        self.source = track

    async def recv(self) -> av.VideoFrame:
        frame = await self.source.recv()
        return frame.reformat(format='yuv420p')
        


async def main():
    try:
        ffplay = None

        # Run the streaming task
        target_name = f'/dev/video{TARGET_INDEX}'
        output = VideoOutputDevice(target_name)
        feeder = CameraFeeder(output, width=640, height=480)
        await feeder.start()

        # start ffplay
        await asyncio.sleep(0.5)
        ffplay = await sp.create_subprocess_exec(
            'ffplay', target_name, '-hide_banner', '-nostats',
            stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE
        )

        # show splash frame
        await wait(3)

        # Set target to /dev/video0 first
        await set_source(feeder, '/dev/video0', 0)
        await wait(3)

        # Set target to /dev/video4 first
        await set_source(feeder, '/dev/video4', 0)
        await wait(3)

        # Set target to /dev/video0 first
        await set_source(feeder, '/dev/video0', 0)
        await wait(3)

        # Set target to None
        await set_source(feeder, None)
        await wait(2)
        return

    finally:
        # Stop ffplay
        if ffplay:
            ffplay.terminate()
            ffout, fferr = await ffplay.communicate('q'.encode())
            print("\n".join((
                "--- FFPLAY stdout -------------------------------------",
                ffout.decode(),
                "--- FFPLAY stdout -------------------------------------",
                fferr.decode(),
                "--- FFPLAY end--- -------------------------------------"
            )))

        # Stop writing to target
        await feeder.stop()
        if AppData.source:
            AppData.source.video.stop()


if __name__ == "__main__":
    
    from components import create_logger

    create_logger().add_stream_target(level = 0)
    asyncio.run(main())
