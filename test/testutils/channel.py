import asyncio
from typing import Optional

import sys
from os.path import dirname
PROJECT_DIR = dirname(dirname(dirname(__file__))) + "/src"
sys.path.insert(0, PROJECT_DIR)

from components.channel import Channel, ChannelClosedError


class MessageQueue(asyncio.Queue):
    def __init__(self, owner: Channel):
        super().__init__()
        self.name = owner.local_name
        self.__closed = asyncio.Event()

    @property
    def is_closed(self) -> bool:
        return self.__closed.is_set()

    def close(self) -> None:
        self.__closed.set()

    async def put(self, msg: bytes) -> None:
        if self.__closed.is_set():
            raise ChannelClosedError()
        await super().put(msg)

    async def get(self, timeout: Optional[float] = None) -> bytes:
        tasks = (
            asyncio.create_task(super().get()),
            asyncio.create_task(self.__closed.wait())
        )
        done, pending = await asyncio.wait(
            tasks,
            timeout=timeout,
            return_when=asyncio.FIRST_COMPLETED
        )
        # Cancel other tasks
        for task in pending: task.cancel()
        await asyncio.gather(*pending, return_exceptions=True)
        # Check the finished task and react accordingly
        if len(done) == 0:      # time out
            raise asyncio.TimeoutError
        done = done.pop()
        if done == tasks[0]:
            return done.result()
        else:
            raise ChannelClosedError()
            


class MockChannel(Channel):

    def __init__(self, name: str = "mock"):
        super().__init__(None, None)
        self.name = name
        self.peer: Optional[MessageQueue] = None
        self.queue = MessageQueue(self)

    @property
    def remote_name(self) -> str:
        return self.peer and self.peer.name
    
    @property
    def local_name(self) -> str:
        return self.name
    
    @property
    def is_closing(self) -> bool:
        return self.peer is None

    @property
    def is_closed(self) -> bool:
        return self.queue.is_closed
    
    async def close(self):
        if self.peer is None: return
        self.queue.close()
        self.peer.close()
        self.peer = None

    async def recv(
            self, /,
            wait_time: float = 0,
            message_timeout: float = 1.0
    ) -> bytes:
        if self.peer is None: raise ChannelClosedError()
        try:
            return await self.queue.get(message_timeout)
        except asyncio.TimeoutError:
            await self.close()
            raise ChannelClosedError
        except Exception:
            await self.close()
            raise

    async def send(self, data: bytes):
        if self.peer is None: raise ChannelClosedError()
        await self.peer.put(data)


def create_channel(nameA='peerA', nameB='peerB'):
    peerA = MockChannel(nameA)
    peerB = MockChannel(nameB)
    peerA.peer = peerB.queue
    peerB.peer = peerA.queue
    return peerA, peerB


if __name__ == "__main__":

    async def main():
        peerA, peerB = create_channel()
        msg = b'test'
        await peerA.send(msg)
        got = await peerB.recv()
        assert got == msg, "must receive what has been sent"

        await peerA.close()
        assert peerB.is_closed, "peerB is not closed"

        print("Channel - OK")

    asyncio.run(main())
