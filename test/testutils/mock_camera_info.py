from typing import Tuple, Set
from video.v4l2utils import CameraInfo

def MockCamera(id: str, index: int, **formats) -> Tuple[CameraInfo, Set[Tuple[int,int]], Set[int], Set[int]]:
    sizes = set()
    widths = set()
    heights = set()

    for pix_format in formats.values():
        for format in pix_format:
            sizes.add(format)
            widths.add(format[0])
            heights.add(format[1])

    return (CameraInfo(
        id=id.ljust(10, "x") + str(index).rjust(2,'0'),
        path=f"/dev/video{index}",
        label=f"Test camera #{index}",
        driver="mock",
        version="1.0.0",
        bus_info="",
        capabilities=None,
        device_capabilities=None,
        formats={ pix_fmt : [ {
            "width": size[0], "height": size[1], "fps": (15,)
        } for size in sizes ] for pix_fmt, sizes in formats.items() }
    ), sizes, widths, heights)

mock_cameras = [
    MockCamera('abcdefghij', 0, YUYV=((640,480), (1024, 720), (1920, 1080)), MJPEG=((640,480), (640,320), (1024,720), (1600,1200))),
    MockCamera('abcdefghij', 1, YUYV=((640,480), (1024, 720), (1920, 1080)), MJPEG=((640,480), (640,320), (1024,720), (1600,1200))),
    MockCamera('xhaysdasde', 2, GRAY=((640,360),)),
    MockCamera('askjfidsuf', 5, YUYV=((800,600),)),
]
