import asyncio
from dataclassy import dataclass
from typing import Any, Set, Coroutine

class TaskSpawner:

    def __init__(self):
        self.cancelled: int = 0
        self.finished: int = 0
        self.tasks: Set[asyncio.Task] = set()

    @property
    def spawned(self):
        return len(self.tasks)

    async def __coro(self, duration: int, value: Any):
        try:
            await asyncio.sleep(duration/1000)
            if isinstance(value, Exception):
                raise value
            return value
        except asyncio.CancelledError:
            self.cancelled += 1
        finally:
            self.finished += 1

    def start_coro(self, duration: int, value: Any = None):
        coro = self.__coro(duration, value)
        self.tasks.add(asyncio.create_task(coro))
        return coro

    def start_task(self, duration: int, value: Any = None):
        task = asyncio.create_task(self.__coro(duration, value))
        self.tasks.add(task)
        return task
    
    async def __aenter__(self):
        return self
    
    async def __aexit__(self, exc, exc_type, tb):
        for task in self.tasks:
            task.done() or task.cancel()
        await asyncio.gather(*self.tasks, return_exceptions=True)


if __name__ == "__main__":

    async def main():
        spawner = TaskSpawner()
        async with spawner:
            spawner.start_coro(duration=10)
            spawner.start_coro(duration=1000)
            spawner.start_task(duration=10)
            spawner.start_task(duration=1000)
            assert spawner.spawned == 4

            await asyncio.sleep(0.1)
            assert spawner.finished == 2

        assert spawner.cancelled == 2

    asyncio.run(main())
    print("TaskSpawner - OK")
