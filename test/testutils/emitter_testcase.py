from unittest import TestCase, IsolatedAsyncioTestCase
from pyee import EventEmitter

class EmitterAssertion:
    def __init__(self, test: TestCase, emitter: EventEmitter, event: str, *, count: int = -1, args = None, kvargs = None):
        self.__test = test
        self.__count = 0
        self.__args = args
        self.__kvargs = kvargs
        self.__expected = count
        self.__emitter = emitter
        self.__event = event

    def increase_counter(self, *args, **kvargs):
        if self.__args is not None and args != self.__args: return
        if self.__kvargs is not None and kvargs != self.__kvargs: return
        self.__count += 1

    def __enter__(self):
        self.__emitter.add_listener(self.__event, self.increase_counter)
        self.__count = 0
        return self
    
    def __exit__(self, *args):
        self.__emitter.remove_listener(self.__event, self.increase_counter)
        if self.__expected < 0:
            self.__test.assertGreaterEqual(self.__count, -self.__expected)
        else:
            self.__test.assertEqual(self.__count, self.__expected)

class EmitterTestCase(IsolatedAsyncioTestCase):
    def assertEmits(self, emitter: EventEmitter, event: str, *, count: int = -1, args = None, kvargs = None):
        return EmitterAssertion(self, emitter, event, count=count, args=args, kvargs=kvargs)

