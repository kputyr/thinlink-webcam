"""
WebRTC streaming test #4

source: /dev/video0
target: /dev/video10

Both sender and receiver are controlled by this script and messages are transfered
directly between them. The received stream is written to the loopback device:
for each frame the internal buffers of planes are dumped to the device.

Once the streaming starts, press 'q' or 'Q' to stop it.
"""

import asyncio
import aiortc
import logging

from aiortc.contrib.media import MediaPlayer, MediaRecorderContext, MediaStreamError
from aiortc.mediastreams import MediaStreamTrack
from sys import stdin, stdout
from io import BufferedWriter

import fcntl
from v4l2py import device as v4l2device
from v4l2py import raw as v4l2


SOURCE_VIDEO="/dev/video0"
TARGET_VIDEO="/dev/video10"
VIDEO_PARAMETERS = {
    "video_size": "640x480",
    "framerate": "30"
}


class V4L2Recorder:
    """
    A media sink that writes audio and/or video to a v4l2 loopback device.
    """

    def __init__(self, device):
        self.__device = device
        self.__track = None
        self.logger = logging.getLogger('rec')
        self.logger.info('created a recorded')

    def addTrack(self, track):
        """
        Add a track to be recorded.

        :param track: A :class:`aiortc.MediaStreamTrack`.
        """
        if track.kind == "video":
            self.__track = track
            self.__task = None
            self.logger.info('added a track')

    async def start(self):
        """
        Start recording.
        """
        if self.__task is None:
            self.__task = asyncio.ensure_future(
                self.__run_track(self.__track, self.__device)
            )
            self.logger.info('started')

    async def stop(self):
        """
        Stop recording.
        """
        if self.__task:
            self.__task.cancel()
            self.__task = None
            self.__track = None
            self.logger.info('stopped')

    async def __run_track(self, track: MediaStreamTrack, context: MediaRecorderContext):
        expected_sizes = [
            640 * 480,       # luma Y plane
            640 * 480 >> 2,  # chroma U plane
            640 * 480 >> 2  # chroma V plane
        ]
        while True:
            try:
                frame = await track.recv()
                # Check the output
                assert len(frame.planes) == 3
                for i in range(0,3):
                    assert frame.planes[i].buffer_size == expected_sizes[i]
                # Save planes directly to the loopback device
                for plane in frame.planes:
                    self.__device.write(plane)
#                self.__device.write(
#                    bytes(frame.planes[0]) +
#                    bytes(frame.planes[1]) +
#                    bytes(frame.planes[2])
#                )
            except MediaStreamError as e:
                self.logger.error(f"Media error: {e}")
                return
            except BaseException as e:
                self.logger.error(f"Unexpected error: {e}")

class Publisher:
    def __init__(self, source: MediaPlayer):
        self.source = source
        self.rtc = None
        self.logger = logging.getLogger('pub')

    def open(self):
        self.logger.info("opening a connection")
        self.rtc = rtc = aiortc.RTCPeerConnection()

        @rtc.on('connectionstatechange')
        async def on_connectionstatechange():
            self.logger.info("connection state is %s" % rtc.connectionState)
            if rtc.connectionState == "failed":
                await rtc.close()

    async def create_offer(self):
        self.logger.info("adding the video track")
        self.rtc.addTrack(self.source.video)
        self.logger.info("creating an offer")
        offer = await self.rtc.createOffer()

        self.logger.info("setting and returning the local description")
        await self.rtc.setLocalDescription(offer)
        return self.rtc.localDescription

    async def accept_answer(self, answer):
        self.logger.info("accepting an answer")
        await self.rtc.setRemoteDescription(answer)
        self.logger.info("the connection is prepared")

    async def close(self):
        self.source.video.stop()
        await self.rtc.close()

        
class Receiver:
    def __init__(self, target: V4L2Recorder):
        self.target = target
        self.rtc = None
        self.logger = logging.getLogger('sub')

    def open(self):
        self.logger.info("opening a connection")
        self.rtc = rtc = aiortc.RTCPeerConnection()

        @rtc.on("connectionstatechange")
        async def on_connectionstatechange():
            self.logger.info("connection state is %s" % rtc.connectionState)
            if rtc.connectionState == "failed":
                await rtc.close()

        @rtc.on("track")
        def on_track(track):
            self.logger.info("received a track: %s" % track.kind)
            if track.kind != "video": return
            self.target.addTrack(track)

            @track.on("ended")
            async def on_ended():
                self.logger.info("Track has ended: %s" % track.kind)
                await self.target.stop()


    async def create_answer(self, offer):
        self.logger.info("accepting an offer")
        await self.rtc.setRemoteDescription(offer)
        await self.target.start()

        self.logger.info("return an answer")
        answer = await self.rtc.createAnswer()
        await self.rtc.setLocalDescription(answer)

        return self.rtc.localDescription

    async def close(self):
        await self.rtc.close()


async def connect_stdio():
    loop = asyncio.get_event_loop()
    astdin = asyncio.StreamReader()
    protocol = asyncio.StreamReaderProtocol(astdin)
    await loop.connect_read_pipe(lambda: protocol, stdin)
    w_transport, w_protocol = await loop.connect_write_pipe(asyncio.streams.FlowControlMixin, stdout)
    astdout = asyncio.StreamWriter(w_transport, w_protocol, astdin, loop)
    return astdin, astdout


async def main():

    astdin, astdout = await connect_stdio()

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    handler = logging.StreamHandler(stdout)
    handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)-6s %(name)-10s %(message)s'))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    source = Publisher(MediaPlayer(
        SOURCE_VIDEO,
        format="v4l2",
        options=VIDEO_PARAMETERS
    ))

    # Prepare the target

    logger.info(f"opening {TARGET_VIDEO}")
    device = BufferedWriter(
        v4l2device.fopen(TARGET_VIDEO, rw=True),
        320*480*3
    )
    
    format = v4l2.v4l2_format()
    logger.info(f"created format object")
    format.type = v4l2.V4L2_BUF_TYPE_VIDEO_OUTPUT
    format.fmt.pix.pixelformat = v4l2.V4L2_PIX_FMT_YUV420
    format.fmt.pix.width = 640
    format.fmt.pix.height = 480
    format.fmt.pix.field = v4l2.V4L2_FIELD_NONE
    format.fmt.pix.bytesperline = 320 * 3   # yuv420 needs 12 bits per pixel
    format.fmt.pix.sizeimage = 320 * 480 * 3
    format.fmt.pix.colorspace = v4l2.V4L2_COLORSPACE_JPEG
    fcntl.ioctl(device, v4l2.VIDIOC_S_FMT, format)
    logger.info("format is set")

    target = Receiver(V4L2Recorder(device))

    source.open()
    target.open()

    # a dirty trick to exchange ICE candidates without using a channel
    source_rtc = source.rtc
    target_rtc = target.rtc
    
    @source_rtc.on("icecandidate")
    def on_source_icecandidate(candidate):
        if candidate:
            target.logger.info("received an ICE candidate")
            target.rtc.addIceCandidate(candidate)

    @target_rtc.on("icecandidate")
    def on_target_icecandidate(candidate):
        if candidate:
            source.logger.info("received an ICE candidate")
            source.rtc.addIceCandidate(candidate)


    offer = await source.create_offer()
    answer = await target.create_answer(offer)
    await source.accept_answer(answer)

    astdout.write(b"\nStreaming. Press q to end\n\n")
    while True:
        ch = await astdin.read(1)
        if ch in (b'q', b'Q'): break
    
    await source.close()
    await target.close()

    device.raw.close()


if __name__ == "__main__":
    print(__doc__)
    asyncio.run(main())
