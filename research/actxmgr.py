import asyncio


class ACtxMgr:
    def __init__(self, name):
        self.name = name

    def log(self, text):
        print(f"[{self.name}] {text}")

    async def __aenter__(self):
        self.log("Entering")
        return self

    async def __aexit__(self, exc_type, exc, trb):
        self.log("Exiting")
        self.log("\tException type: {}".format(exc_type))
        self.log("\tException value: {} (type {})".format(exc, type(exc)))
        self.log("\tTrace back: {}".format(trb))


async def test():
    async with ACtxMgr('test1') as ctx:
        ctx.log("inside!")

    async with ACtxMgr('test2') as ctx:
        ctx.log("raising!")
        raise Exception('Because I can')



if __name__ == "__main__":
    asyncio.run(test())