from subprocess import Popen, PIPE
from sys import stdout, stderr, stdin

print("Starting echo.sh")
process = Popen(['./echo.sh'], stdin=stdin, stdout=stdout, stderr=stderr)
print("Running echo.sh")
process.wait()
print("Stopped echo.sh")
