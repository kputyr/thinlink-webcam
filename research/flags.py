from aiortc import VideoStreamTrack
from aiortc.mediastreams import MediaStreamError
from av import VideoFrame
from av.frame import Frame

from typing import List, Iterator

class GeneratedVideoStreamTrack(VideoStreamTrack):
    def __init__(self, width: int, height: int) -> None:
        super().__init__()
        self.__current = 0
        self.__frames: List[VideoFrame] = list()
        for frame_data in self.generate():
            frame = VideoFrame(width, height)
            for p in frame.planes:
                p.update
            self.__frames.append(frame)


    def generate(self) -> Iterator[bytes]:
        raise NotImplementedError

    async def recv(self) -> Frame:
        pts, time_base = await self.next_timestamp()

        frame  = self.__frames[self.__current]
        frame.pts = pts
        frame.time_base = time_base
        self.__current += 1
        if self.__current == len(self.__frames):
            self.__current = 0
        return frame
