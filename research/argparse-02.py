"""
A program for testing the moment when an object is converted
to a string by ArgumentParser.
"""

import argparse

commands = {
    "stop": "This stops the program.",
    "echo": "This should print all the arguments.\nIs this another line?"
}

class CustomParser(argparse.ArgumentParser):
    def format_help(self):
        extra = "\n\n".join(
            "\n".join((cmd, "~" * len(cmd), desc)) for cmd, desc in commands.items()
        )
        return super().format_help() + "\n\n" + extra

#    def print_help(self):
#        super().print_help()
#        for cmd, desc in commands.items():
#            print("\n".join((
#                "", cmd, "~" * len(cmd), desc
#            )))
#        print("")

parser = CustomParser(
    description="This is the description",
    epilog="This is the epilog. It documents all additional parameters.\nIs this the same line?"
)
parser.add_argument(
    "-a",  "--admin",
    help="returns the admin's account",
    action="store_true"
)
parser.add_argument(
    "-c", "--coder",
    help="returns the name of the coder",
    action="store_true"
)
parser.add_argument(
    "command",
    help="a command to be invoked",
    metavar='command [argument]',
    choices=commands.keys()
)
args = parser.parse_args()

print(f"Invoking {args.command}")
if args.admin:
    print("Admin's account: admin")
if args.coder:
    print("Coder's name is too hard to spell, sorry!")
