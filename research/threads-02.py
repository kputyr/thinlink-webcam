import time
import threading
import asyncio


async def side_task(time):
    print("side task: start")
    await asyncio.sleep(time/2)
    print("side task: middle")
    await asyncio.sleep(time/2)
    print("side task: stop")

def thread_task(time_, loop):

    def mark_status(fut):
        done = "YES" if fut.done() else "NO"
        print(f"side task future is done: {done}")

    print("side task: start")
    fut = asyncio.run_coroutine_threadsafe(side_task(time_), loop)
    mark_status(fut)
    time.sleep(time_/2)
    print("side task: middle")
    mark_status(fut)
    time.sleep(time_/2)
    print("side task: stop")
    mark_status(fut)

async def main(time):
    thread = threading.Thread(
        target = thread_task,
        name="test thread",
        args=(time, asyncio.get_running_loop()),
        daemon=True
    )
    print("main task: start")
    thread.start()
    await asyncio.sleep(time/2)
    print("main task: middle")
    await asyncio.sleep(time/2)
    print("main task: stop")
    thread.join()

if __name__ == "__main__":
    asyncio.run(main(2.0))