import threading
import ctypes
import trace
import sys
from typing import Optional

class CancelledError(Exception):
    pass

class CancellableThread(threading.Thread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cancelled = False
        self._throw = None
    
    def __debug(self, msg: str):
        print(f"[{self.name}] {msg}")

    def run(self):
        try:
            self.__debug('started')
#            sys.settrace(self.globaltrace)
            super().run()
        except CancelledError:
            self.cancelled = True
            self.__debug('cancelled')
        finally:
            self.__debug('finished')

    def globaltrace(self, frame, event, arg):
        return self.localtrace if event == 'call' else None
    
    def localtrace(self, frame, event, args):
        if self._throw and event == 'line': raise self._throw
        return self.localtrace

    @property
    def thread_id(self) -> Optional[int]:
        if hasattr(self, '_thread_id'):
            return self._thread_id
        for id, thread in threading._active.items():
            if thread is self:
                return id
        return None

    def throw__(self, exc: Exception):
        self._throw = exc

    def throw(self, exc: Exception):
        self.__debug(f'throwing {exc}')
        thread_id = ctypes.c_long(self.thread_id)
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(
            thread_id,
            ctypes.py_object(exc)
        )
        if res > 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
            self.__debug(f'failed to throw {exc}')
        else:
            self.__debug(f'thrown {exc}')

    def stop(self):
        self.__debug(f'stopping')
        self.throw(SystemExit())


def test_thread(name, target):
    print(f"TEST {name} START")
    thread = CancellableThread(target=target, name=name)
    thread.start()
    time.sleep(0.5)
    thread.stop()
    thread.join()
    print(f"TEST {name} END")

if __name__ == "__main__":

    import time
    import socket

    def loop_task():
        while True:
            time.sleep(1)

    test_thread('loop', loop_task)

    test_thread('sleep', lambda: time.sleep(60))

    socketA, socketB = socket.socketpair()
    file = socketA.makefile('rb')
    test_thread('read', file.readline)
    socketA.close()
    socketB.close()

