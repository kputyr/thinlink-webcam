import asyncio


async def delayed_print(text: str, delay: float=0):
    await asyncio.sleep(delay)
    print(f"[{delay}] {text}")

def sync_print(text: str, delay: float=0):
    loop = asyncio.get_event_loop()
    loop.run_until_complete(delayed_print(text, delay))

async def main():
    print("Inside a function")
    task1 = asyncio.create_task(delayed_print("First", 0.5))
    task2 = asyncio.create_task(delayed_print("Second", 0.25))
    sync_print("Third", 0.75)

if __name__ == "__main__":
    loop = asyncio.new_event_loop()
    task1 = loop.create_task(delayed_print("First", 0.5))
    task2 = loop.create_task(delayed_print("Second", 0.25))
    loop.run_until_complete(delayed_print("Third", 0.75))
    loop.close()

    asyncio.run(main())
