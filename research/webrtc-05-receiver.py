"""
WebRTC streaming test #4

source: /dev/video0
target: /dev/video10

Both sender and receiver are controlled by this script and messages are transfered
directly between them. The received stream is written to the loopback device:
for each frame the internal buffers of planes are dumped to the device.

Once the streaming starts, press 'q' or 'Q' to stop it.
"""

import argparse
import re
import socket

import asyncio
import aiortc
import logging

from aiortc.contrib.media import MediaPlayer, MediaRecorderContext, MediaStreamError
from aiortc.mediastreams import MediaStreamTrack
from sys import stdin, stdout
from io import BufferedWriter
from typing import Tuple, Optional

import fcntl
from v4l2py import device as v4l2device
from v4l2py import raw as v4l2

HOST='127.0.0.1'
PORT=8888

TARGET_VIDEO="/dev/video10"

ACTION_ERROR = "error"
ACTION_START = "start"
ACTION_STOP = "stop"
ACTION_OFFER = "offer"
ACTION_ANSWER = "answer"


def addr2hostport(addr: Tuple[str,int]) -> str:
    return f"{addr[0]}:{addr[1]}"


class ChannelException(Exception):
    pass

class ChannelClosedError(ChannelException):
    pass

class ChannelAuthenticationFailed(ChannelException):
    pass

class ChannelProtocolError(ChannelException):
    pass

class ChannelIncompleteMessage(ChannelException):
    def __init__(self, expected, read, command):
        super().__init__()
        self.expected = expected
        self.read = read
        self.command = command


class Channel:

    def __init__(self):
        self.reader: Optional[asyncio.StreamReader] = None
        self.writer: Optional[asyncio.StreamWriter] = None
        self.logger = logging.getLogger('client')

    async def open(self, host: str, port: int):
        self.reader, self.writer = await asyncio.open_connection(host, port)
        addr = self.writer.get_extra_info('peername')
        self.logger.info(f"connected to {addr[0]}:{addr[1]}")

    @property
    def is_closed(self):
        return self.writer is None or self.writer.is_closing()

    async def close(self):
        if self.is_closed: return
        writer = self.writer
        self.writer = None
        self.reader = None
        writer.close()
        await writer.wait_closed()
        self.logger.info("x")

    async def __read(self, size: int, initial: bool = False) -> bytes:
        if self.reader is None: raise ChannelClosedError()
        try:
            return await self.reader.readexactly(size)
        except asyncio.IncompleteReadError as err:
            read = len(err.partial)
            if initial and read == 0:
                raise ChannelClosedError()
            else:
                raise ChannelIncompleteMessage(size, read)
        except ConnectionResetError:
            raise ChannelClosedError()
    
    async def recv(self) -> Tuple[str, str]:
        header = await self.__read(8, True)
        if header[0:4] != b'RWP1':
            raise ChannelProtocolError("missing header")
        size = int.from_bytes(header[4:], 'little')
        message = (await self.__read(size)).decode().split(':',1)
        if len(message) < 2:
            raise ChannelProtocolError("missing command")
        command, data = message
        self.logger.info(f'< {command}')
        return command, data

    async def send(self, command: str, data: str):
        self.logger.info(f'> {command}')
        if self.is_closed: raise ChannelClosedError()
        encoded = f"{command}:{data}".encode()
        size = len(encoded)
        self.writer.write(b"RWP1")
        self.writer.write(size.to_bytes(4, 'little'))
        self.writer.write(encoded)
        await self.writer.drain()


class V4L2Recorder:
    """
    A media sink that writes audio and/or video to a v4l2 loopback device.
    """

    def __init__(self, device):
        self.__device = device
        self.__track = None
        self.logger = logging.getLogger('rec')
        self.logger.info('created a recorded')

    def addTrack(self, track):
        """
        Add a track to be recorded.

        :param track: A :class:`aiortc.MediaStreamTrack`.
        """
        if track.kind == "video":
            self.__track = track
            self.__task = None
            self.logger.info('added a track')

    async def start(self):
        """
        Start recording.
        """
        if self.__task is None:
            self.__task = asyncio.ensure_future(
                self.__run_track(self.__track, self.__device)
            )
            self.logger.info('started')

    async def stop(self):
        """
        Stop recording.
        """
        if self.__task:
            self.__task.cancel()
            self.__task = None
            self.__track = None
            self.logger.info('stopped')

    async def __run_track(self, track: MediaStreamTrack, context: MediaRecorderContext):
        expected_sizes = [
            640 * 480,       # luma Y plane
            640 * 480 >> 2,  # chroma U plane
            640 * 480 >> 2  # chroma V plane
        ]
        while True:
            try:
                frame = await track.recv()
                # Check the output
                assert len(frame.planes) == 3
                for i in range(0,3):
                    assert frame.planes[i].buffer_size == expected_sizes[i]
                # Save planes directly to the loopback device
                for plane in frame.planes:
                    self.__device.write(plane)
#                self.__device.write(
#                    bytes(frame.planes[0]) +
#                    bytes(frame.planes[1]) +
#                    bytes(frame.planes[2])
#                )
            except MediaStreamError as e:
                self.logger.error(f"Media error: {e}")
                return
            except BaseException as e:
                self.logger.error(f"Unexpected error: {e}")


class Receiver:
    def __init__(self, target: V4L2Recorder):
        self.target = target
        self.rtc = None
        self.logger = logging.getLogger('sub')

    def open(self):
        self.logger.info("opening a connection")
        self.rtc = rtc = aiortc.RTCPeerConnection()

        @rtc.on("connectionstatechange")
        async def on_connectionstatechange():
            self.logger.info("connection state is %s" % rtc.connectionState)
            if rtc.connectionState == "failed":
                await rtc.close()

        @rtc.on('icecandidate')
        async def on_ice_candidate(candidate):
            self.logger.info("Found an ICE candidate")

        @rtc.on("track")
        def on_track(track):
            self.logger.info("received a track: %s" % track.kind)
            if track.kind != "video": return
            self.target.addTrack(track)

            @track.on("ended")
            async def on_ended():
                self.logger.info("Track has ended: %s" % track.kind)
                await self.target.stop()


    async def create_answer(self, offer):
        self.logger.info("accepting an offer")
        await self.rtc.setRemoteDescription(offer)
        await self.target.start()

        self.logger.info("return an answer")
        answer = await self.rtc.createAnswer()
        await self.rtc.setLocalDescription(answer)

        return self.rtc.localDescription

    async def close(self):
        await self.rtc.close()



async def connect_stdio():
    loop = asyncio.get_event_loop()
    astdin = asyncio.StreamReader()
    protocol = asyncio.StreamReaderProtocol(astdin)
    await loop.connect_read_pipe(lambda: protocol, stdin)
    w_transport, w_protocol = await loop.connect_write_pipe(asyncio.streams.FlowControlMixin, stdout)
    astdout = asyncio.StreamWriter(w_transport, w_protocol, astdin, loop)
    return astdin, astdout


async def main():

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "-a", "--addr",
        help=f"The address of the server in the format host:port, each component is optional. Default: {HOST}:{PORT}",
        type=str,
        required=False
    )
    parser.add_argument(
        "commands",
        help="Commands and names. Commands ends with a colon ':'",
        default="World",
        type=str,
        nargs='*'
    )
    args = parser.parse_args()

    # Convert host to an IP address if necessary
    host = HOST
    port = PORT
    if args.addr:
        addr = args.addr.split(':', 1)
        if addr[0]:
            ip_match = re.match(r"(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})", addr[0])
            if ip_match is None or any((int(n) in range(0,256) for n in ip_match.groups())):
                host = socket.gethostbyname(addr[0])
            else:
                host = addr[0]
        if len(addr) > 1:
            port = int(addr[1])


    astdin, astdout = await connect_stdio()

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    handler = logging.StreamHandler(stdout)
    handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)-7s %(name)-10s %(message)s'))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    # Prepare the target

    logger.info(f"opening {TARGET_VIDEO}")
    device = BufferedWriter(
        v4l2device.fopen(TARGET_VIDEO, rw=True),
        320*480*3
    )
    
    format = v4l2.v4l2_format()
    logger.info(f"created format object")
    format.type = v4l2.V4L2_BUF_TYPE_VIDEO_OUTPUT
    format.fmt.pix.pixelformat = v4l2.V4L2_PIX_FMT_YUV420
    format.fmt.pix.width = 640
    format.fmt.pix.height = 480
    format.fmt.pix.field = v4l2.V4L2_FIELD_NONE
    format.fmt.pix.bytesperline = 320 * 3   # yuv420 needs 12 bits per pixel
    format.fmt.pix.sizeimage = 320 * 480 * 3
    format.fmt.pix.colorspace = v4l2.V4L2_COLORSPACE_JPEG
    fcntl.ioctl(device, v4l2.VIDIOC_S_FMT, format)
    logger.info("format is set")

    receiver = Receiver(V4L2Recorder(device))
    streaming = False

    channel = Channel()
    await channel.open(host, port)

    menu = "\n".join([
        "\nCommands",
        "\tq - quit",
        "\ts - start stream",
        "\tc - close stream\n\n"
    ]).encode()

    while not channel.is_closed:
        try:
            astdout.write(menu)
            ch = await astdin.read(1)
            if ch in (b'q', b'Q'):
                await channel.close()

            elif ch in (b's', b'S') and not streaming:
                
                receiver.open()
                await channel.send(ACTION_START, "")
                command, data = await channel.recv()
                if command != ACTION_OFFER:
                    await receiver.close()
                    raise Exception(f"unexpected command: {command}")
                answer = await receiver.create_answer(
                    aiortc.RTCSessionDescription(data, "offer")
                )
                await channel.send(ACTION_ANSWER, answer.sdp)
                streaming = True
                logger.info("streaming")

            elif ch in (b'c', b'C') and streaming:

                streaming = False
                await receiver.close()
                await channel.send(ACTION_STOP, "")

        except Exception as err:
            logger.error(err)
            if not streaming:
                await receiver.close()

    if not channel.is_closed:
        await channel.close()
    if streaming:
        await receiver.close()
    device.raw.close()


if __name__ == "__main__":
    print(__doc__)
    asyncio.run(main())
