"""
WebRTC signaling channel test #2 (client)

Connects to a signaling server using a TCP socket and 
sends several messages at the same time. The messages
are encoded following a simple protocol that provides
the size of the encoded message.

For testing purposes the message can be sent in parts
with several second pauses, which allows to test how
the server behaves when a connection is lost before
an entire message is received.
"""

import asyncio
import logging
from typing import List, Tuple

from utils import addr2hostport


DEFAULT_HOST = ('127.0.0.1', 8888)


class ChannelException(Exception):
    pass

class ChannelClosedError(ChannelException):
    pass

class ChannelAuthenticationFailed(ChannelException):
    pass

class ChannelProtocolError(ChannelException):
    pass

class ChannelIncompleteMessage(ChannelException):
    def __init__(self, expected, read, command):
        super().__init__()
        self.expected = expected
        self.read = read
        self.command = command


class Channel:
    def __init__(
        self,
        reader: asyncio.StreamReader,
        writer: asyncio.StreamWriter
    ):
        self.reader = reader
        self.writer = writer
        self.id = addr2hostport(writer.get_extra_info('peername'))
        self.logger = logging.getLogger(self.id)
        self.__close_event = None

    @classmethod
    async def open(cls, host: str, port: int=-1):
        opener = asyncio.open_unix_connection(host) if port < 0 \
            else asyncio.open_connection(host, port)
        reader, writer = await opener
        return cls(reader, writer)

    @property
    def is_closing(self):
        return self.writer is None

    async def close(self):
        if self.writer is None:
            await self.__close_event()
        else:
            self.__close_event = asyncio.Event()
            writer = self.writer
            self.writer = None
            self.reader = None
            writer.close()
            try: # This may fail (BrokenPipeError)
                await writer.wait_closed()
            except Exception as err:
                self.logger.error(err)
            self.__close_event.set()
            self.logger.info("x")
    
    # Safe read from the channel
    async def __read(self, size: int, initial: bool = False) -> bytes:
        if self.reader is None: raise ChannelClosedError()
        try:
            return await self.reader.readexactly(size)
        except asyncio.IncompleteReadError as err:
            read = len(err.partial)
            if initial and read == 0:
                raise ChannelClosedError()
            else:
                raise ChannelIncompleteMessage(size, read)
        except ConnectionResetError:
            raise ChannelClosedError()

    async def recv(self) -> Tuple[str, str]:
        header = await self.__read(8, True)
        if header[0:4] != b'RWP1':
            raise ChannelProtocolError("invalid header")
        size = int.from_bytes(header[4:], 'little')
        message = (await self.__read(size)).decode().split(':',1)
        if len(message) < 2:
            self.logger.info(f"< {message[0]}")
            return message[0], None
        else:
            self.logger.info(f"< {message[0]} {message[1]}")
            return message

    async def send(self, command: str, data: str, slow=0):
        self.logger.info(f'> {data} ({command})')
        if self.is_closing:
            self.logger.warning('aleady closed')
            return
        encoded = f"{command}:{data}".encode()
        size = len(encoded)
        self.writer.write(b"RWP1")
        if slow: await asyncio.sleep(slow)
        self.writer.write(size.to_bytes(4, 'little'))
        if slow: await asyncio.sleep(slow)
        self.writer.write(encoded)
        try: # This may fail (BrokenPipeError)
            await self.writer.drain()
        except Exception as err:
            self.logger.exception(err)


async def make_request(host: str, port: int, data: List[str]):    
    # open a channel
    channel = await Channel.open(host, port)
    # Send messages    
    command = "greet"
    names = 0
    if len(data) == 0: data = ['World']

    for item in data:
        if item.endswith(':'):
            command = item[0:-1]
        else:
            names += 1
            channel.logger.info(f"> {command}: {item}")
            await channel.send(command, item)
    # Receive responses
    for _ in range(0, names):
        command, response = await channel.recv()
        channel.logger.info(f"< {response} ({command})")

    await channel.close()



if __name__ == "__main__":

    import argparse
    from utils import parse_host, create_logger

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "-a", "--addr",
        help=f"The address of the server in the format host:port, each component is optional. Default: {DEFAULT_HOST[0]}:{DEFAULT_HOST[1]}",
        type=str,
        required=False
    )
    parser.add_argument(
        "commands",
        help="Commands and names. Commands ends with a colon ':'",
        default=["World"],
        type=str,
        nargs='*'
    )
    args = parser.parse_args()
    host, port = parse_host(args.addr, default=DEFAULT_HOST)

    create_logger()
    asyncio.run(make_request(host, port, args.commands))
