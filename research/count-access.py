import subprocess
from typing import List, Dict, Tuple, NamedTuple, Optional

from os import unlink


# The output of the fuser command with the -v option is containing the following fields:
#
#    USER:- user which owns the process.
#    PID:- PID of the process.
#    ACCESS:- Access type.
#    COMMAND:- Command which using the mentioned file or folder.
#
# In the ACCESS column, the access type is represented using the following letters:
#
#    c :- represents the current directory.
#    e :- The file is executable, and it is currently running.
#    f :- open file.  f is omitted in default display mode.
#    F :- open file for writing.  F is omitted in default display mode.
#    r :- represents root directory.
#    m :- mapped file or shared library.
#    . :- Placeholder, omitted in default display mode.
#
# source: https://www.geeksforgeeks.org/fuser-command-in-linux/

class FuserData(NamedTuple):
    user: str
    pid: int
    command: str

    @classmethod
    def from_lsof(cls, data: str):
        user, pid, access, process = data.split()
        return cls(user, int(pid), process)


def fuser(filename: str) -> Dict[str, List[FuserData]]:
    result = subprocess.run(
        'fuser -v ' + filename,
        stderr=subprocess.STDOUT,
        stdout=subprocess.PIPE,
        text=True,
        shell=True
    )
    print("--FUSER START--")
    print(result.stdout)
    print("--FUSER END--")
    data = {}
    if result.returncode == 0:
        current_list: Optional[List[FuserData]] = None
        for line in result.stdout.splitlines():
            entries = line.split()
            count_entries = len(entries)
            if count_entries == 1:
                current_list = list()
                data[entries[0]] = current_list
                continue
            elif count_entries == 4:
                if current_list is None: continue
                user, pid, access, process = entries
            elif count_entries == 5:
                file, user, pid, access, process = entries
                current_list = list()
                data[file] = current_list
            current_list.append(FuserData(user, int(pid), process))
    return data


class TestFile(NamedTuple):
    name: str
    modes: List[str]

def test_fuser():
    files = [
        TestFile('/test01.dat', ['r']),
        TestFile('/test02.dat', ['w']),
        TestFile('/test03.dat', ['r', 'w'])
    ]
    print('Creating files')
    for file in files:
        with open(file.name, 'w') as f:
            f.write('testtesttesttest')

    print('Opening files')
    handlers = []
    for file in files:
        for mode in file.modes:
            handlers.append(open(file.name, mode))

    try:
        print('Fetching data')
        result = fuser('/test*')

    finally:
        print('Closing files')
        for handle in handlers:
            handle.close()

        print('Removing files')
        for file in files:
            unlink(file.name)

        print("Fetched data:")
        for filename, processes in result.items():
            print(filename)
            for data in processes:
                print(f"\t{data.user}\t{data.pid}: {data.command}")


if __name__ == "__main__":
    test_fuser()
