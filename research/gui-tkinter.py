from tkinter import *
from tkinter.ttk import *

from typing import Optional

APP_TITLE = "ThinLinc webcam controller"
BTN_TEXT_STREAMING = "Stop streaming"
BTN_TEXT_NOT_STREAMING = "Start streaming"

CAMERA_SELECTOR_PROMPT = "-- choose a camera --"

class CameraPreview(Frame):
    def __init__(self, parent):
        super().__init__(parent, borderwidth=1)
    
    def display_video_frame(self, frame):
        pass


class CameraSelector(OptionMenu):
    def __init__(self, parent):
        self.__selection = StringVar(parent)
        self.__selected_id = None
        self.__ids = list()
        super().__init__(parent, self.__selection, CAMERA_SELECTOR_PROMPT)
        self.__menu: Menu = self['menu']

    @property
    def selected(self) -> Optional[str]:
        return self.__selected_id
    
    def __index_by_id(self, id: str) -> int:
        return next( (i for i,iid in enumerate(self.__ids) if id==iid), -1)

    def select(self, id: Optional[str]):
        if id is None:
            self.__selected_id = None
            self.__selection.set(CAMERA_SELECTOR_PROMPT)
        else:
            index = self.__index_by_id(id)
            if index < 0: return
            self.__selected_id = id
            self.__selection.set(self.__menu.entrycget(index, 'label'))
    
    def set_cameras(self, cameras):
        self.__selection.set(CAMERA_SELECTOR_PROMPT)
        self.__menu.delete(0, 'end')
        self.__ids = [camera[0] for camera in cameras]
        for id, label in cameras:
            self.__menu.add_command(label=label, command=lambda: self.select(id))

    def add_camera(self, id: str, label: str):
        self.__ids.append(id)
        self.__menu.add_command(label=label, command=lambda: self.select(id))

    def remove_camera(self, id: str):
        index = next( (i for i,iid in enumerate(self.__ids) if id==iid), -1)
        if index < 0: return
        if id == self.__selected_id:
            self.__selected_id = None
            self.__selection.set(CAMERA_SELECTOR_PROMPT)
        self.__ids.pop(index)
        self.__menu.delete(index, index)


class TLWebcam(Tk):
    def __init__(self):
        super().__init__()
        self.cameras = ("Integrated camera", "Rearside camera", "Virtual camera")
        self.streaming = False
        self.title(APP_TITLE)
        self.resizable(False, False)
        self.create_widgets()

    def create_widgets(self):
        # Preview of the camera
        self.frm_preview = Frame(self, width=320, height=240, relief=GROOVE, borderwidth=3)
        self.frm_preview.grid(row=0, padx=5, pady=5)
        # Camera selector
        self.selected_camera = StringVar(self)
        self.opt_camera = OptionMenu(self, self.selected_camera, "--choose a camera--", *self.cameras)
        self.opt_camera.grid(row=1, pady=5)
        # Stream button
        self.btn_stream = Button(self, text=BTN_TEXT_NOT_STREAMING, command=self.on_button_clicked)
        self.btn_stream.grid(row=2, pady=5)

    def on_button_clicked(self):
        self.streaming = not self.streaming
        self.btn_stream['text'] = BTN_TEXT_STREAMING if self.streaming else BTN_TEXT_NOT_STREAMING

if __name__ == "__main__":
    TLWebcam().mainloop()
