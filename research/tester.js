video = document.body.appendChild(document.createElement('video'));
video.controls=true;
Object.assign(video.style, {
  display: 'block',
  width: '100%'
}) 

async function play(id) {
  if (video.srcObject) video.srcObject.getTracks().forEach(t => t.stop());
  if (typeof id === 'number' && window.devices[id])
    id = window.devices[id].deviceId;
  if (typeof id === 'string') {
    video.srcObject = await navigator.mediaDevices.getUserMedia({
      audio:false,
      video: { deviceId: { exact: id } }
    });
    if (video.paused) video.play();
  } else {
    video.srcObject = null;
  }
}

async function updateDevices() {
  window.devices = await navigator.mediaDevices.enumerateDevices();
  console.table(window.devices);
}

updateDevices()
