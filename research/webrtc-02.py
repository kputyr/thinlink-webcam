"""
WebRTC streaming test #2

source: /dev/video0
target: null

Both sender and receiver are controlled by this script and messages are transfered
directly between them. The received stream is nowhere saved, but properties of each
frame are printed to the console.

Once the streaming starts, press 'q' or 'Q' to stop it.
"""

import asyncio
import aiortc
import logging

from aiortc.contrib.media import MediaPlayer, MediaRecorderContext
from aiortc.mediastreams import MediaStreamError, MediaStreamTrack
from sys import stdin, stdout
import av

from typing import NamedTuple

SOURCE_VIDEO="/dev/video0"
VIDEO_PARAMETERS = {
#    "input_format": "mjpeg",
#    "video_size": "640x480",
    "framerate": "20"
}


class TrackAnalyzer(MediaStreamTrack):
    def __init__(self, name: str, track: MediaStreamTrack):
        super().__init__()
        self.track = track
        self.name = name
        self.width = 0
        self.height = 0
        self.format_name = ""
        self.frame_index = 0
        self.delay_print = 0
        self.kind = track.kind
    
    async def recv(self) -> av.VideoFrame:

        try:
            frame = await self.track.recv()
        except Exception as err:
            print(f"[{self.name}] exception! {repr(err)}")
            self.stop()
            raise
        
        self.frame_index += 1
        if self.delay_print == 10:
            self.delay_print = 1
        else:
            self.delay_print += 1
            
        update = self.delay_print == 1 or \
                frame.width != self.width or \
                frame.height != self.height or \
                frame.format.name != self.format_name
            
        if update:
            self.width = frame.width
            self.height = frame.height
            self.format_name = frame.format.name

            timestamp = round(frame.pts * frame.time_base.numerator / frame.time_base.denominator, 3)

            planes = " : ".join((str(plane.buffer_size) for plane in frame.planes))

            key_frame = "key frame" if frame.key_frame else "regular"
            print(f"[{self.name}] {self.frame_index} @ {timestamp} - {self.width}x{self.height} {self.format_name} ({key_frame}) [{planes}]")

        return frame
    
    def stop(self) -> None:
        print(f"[{self.name}] stopping the internal track")
        self.track.stop()
        print(f"[{self.name}] stopping itself")
        super().stop()


class MediaAnalyzer:

    def __init__(self):
        self.logger = logging.getLogger('rec')
        self.logger.info('created')
        self.__task = None

    def addTrack(self, track):
        """
        Add a track to be recorded.

        :param track: A :class:`aiortc.MediaStreamTrack`.
        """
        if track.kind == "video" and self.__task is None:
            self.__task = asyncio.create_task(
                self.start(TrackAnalyzer('recv', track))
            )

    async def start(self, track: MediaStreamTrack):
        """
        Start recording.
        """

        self.logger.info('starts analyzing')
        try:
            while True:
                await track.recv()
        except asyncio.CancelledError:
            track.stop()
        except MediaStreamError:
            self.logger.info('the analyzer track has stopped')
        except Exception as err:
            self.logger.exception(err)


    async def stop(self):
        """
        Stop recording.
        """
        if self.__task is None: return
        task = self.__task
        self.__task = None
        self.logger.info('stops recording')
        task.cancel()
        await task



class Publisher:
    def __init__(self, source: MediaPlayer):
        self.source = source
        self.rtc = None
        self.logger = logging.getLogger('pub')

    def open(self):
        self.logger.info("opening a connection")
        self.rtc = rtc = aiortc.RTCPeerConnection()

        @rtc.on('connectionstatechange')
        async def on_connectionstatechange():
            self.logger.info("connection state is %s" % rtc.connectionState)
            if rtc.connectionState == "failed":
                await rtc.close()

    async def create_offer(self):
        self.logger.info("adding the video track")
        self.rtc.addTrack(TrackAnalyzer('send', self.source.video))
        self.logger.info("creating an offer")
        offer = await self.rtc.createOffer()

        self.logger.info("setting and returning the local description")
        await self.rtc.setLocalDescription(offer)
        return self.rtc.localDescription

    async def accept_answer(self, answer):
        self.logger.info("accepting an answer")
        await self.rtc.setRemoteDescription(answer)
        self.logger.info("the connection is prepared")

    async def close(self):
        await self.rtc.close()


class Receiver:
    def __init__(self, target: MediaAnalyzer):
        self.target = target
        self.rtc = None
        self.logger = logging.getLogger('sub')

    def open(self):
        self.logger.info("opening a connection")
        self.rtc = rtc = aiortc.RTCPeerConnection()

        @rtc.on("connectionstatechange")
        async def on_connectionstatechange():
            self.logger.info("connection state is %s" % rtc.connectionState)
            if rtc.connectionState == "failed":
                await rtc.close()

        @rtc.on("track")
        def on_track(track):
            self.logger.info("received a track: %s" % track.kind)
            if track.kind != "video": return
            self.target.addTrack(track)

            @track.on("ended")
            async def on_ended():
                self.logger.info("Track has ended: %s" % track.kind)
                await self.target.stop()


    async def create_answer(self, offer):
        self.logger.info("accepting an offer")
        await self.rtc.setRemoteDescription(offer)
#        await self.target.start()

        self.logger.info("return an answer")
        answer = await self.rtc.createAnswer()
        await self.rtc.setLocalDescription(answer)

        return self.rtc.localDescription

    async def close(self):
        await self.rtc.close()


async def connect_stdio():
    loop = asyncio.get_event_loop()
    astdin = asyncio.StreamReader()
    protocol = asyncio.StreamReaderProtocol(astdin)
    await loop.connect_read_pipe(lambda: protocol, stdin)
    w_transport, w_protocol = await loop.connect_write_pipe(asyncio.streams.FlowControlMixin, stdout)
    astdout = asyncio.StreamWriter(w_transport, w_protocol, astdin, loop)
    return astdin, astdout


async def main(source_path, options):

    astdin, astdout = await connect_stdio()

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    handler = logging.StreamHandler(stdout)
    handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)-6s %(name)-10s %(message)s'))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    source = Publisher(MediaPlayer(
        source_path,
        format="v4l2",
        options=options
    ))

    target = Receiver(MediaAnalyzer())

    source.open()
    target.open()

    # a dirty trick to exchange ICE candidates without using a channel
    source_rtc = source.rtc
    target_rtc = target.rtc
    
    @source_rtc.on("icecandidate")
    def on_source_icecandidate(candidate):
        if candidate:
            target.logger.info("received an ICE candidate")
            target.rtc.addIceCandidate(candidate)

    @target_rtc.on("icecandidate")
    def on_target_icecandidate(candidate):
        if candidate:
            source.logger.info("received an ICE candidate")
            source.rtc.addIceCandidate(candidate)


    offer = await source.create_offer()
    answer = await target.create_answer(offer)
    await source.accept_answer(answer)

    astdout.write(b"\nStreaming. Press q to end\n\n")
    while True:
        ch = await astdin.read(1)
        if ch in (b'q', b'Q'): break

    logger.info('closing the source')    
    await source.close()
    logger.info('source is closed')

    await asyncio.sleep(0.5)
    logger.info('closing the target')
    await target.close()
    logger.info('target is closed')


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "--format",
        help="Desired input format (mjpeg, rawvideo, etc.)",
        default=None
    )
    parser.add_argument(
        "--pix_fmt",
        help="Pixel format: yuv420p, yuyv422, etc.",
        default=None
    )
    parser.add_argument(
        "-s", "--size",
        help="Size of the video in format {width}x{height}",
        default=None
    )
    parser.add_argument(
        "-d", "--device",
        help="The path to the device",
        default=SOURCE_VIDEO
    )
    args = parser.parse_args()

    print(__doc__)
    options = VIDEO_PARAMETERS
    if args.format: options['input_format'] = args.format
    if args.pix_fmt: options['pix_fmt'] = args.pix_fmt
    if args.size: options['video_size'] = args.size
    asyncio.run(main(args.device, options))

