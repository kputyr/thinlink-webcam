"""
WebRTC signaling channel test #2 (server)

Opens a TCP socket and responds 'Hello' to all incoming messages.
The messages are encoded following a simple protocol that provides
the size of the encoded message.

Usage:
   channel-02-server.py [addr][:port]
By default connects to 127.0.0.1:8888
"""

import asyncio
import logging
from typing import Tuple, Set

from utils import addr2hostport

DEFAULT_HOST = ('127.0.0.1', 8888)

ACTION_ERROR = " err"
ACTION_CLOSE = " close"
ACTION_GREET = "greet"
ACTION_BYE = "bye"
ACTION_FIRE = "fire"
ACTION_NIGHT = "night"



class ChannelException(Exception):
    pass

class ChannelClosedError(ChannelException):
    pass

class ChannelAuthenticationFailed(ChannelException):
    pass

class ChannelProtocolError(ChannelException):
    pass

class ChannelIncompleteMessage(ChannelException):
    def __init__(self, expected, read, command):
        super().__init__()
        self.expected = expected
        self.read = read
        self.command = command


class Channel:
    def __init__(
        self,
        reader: asyncio.StreamReader,
        writer: asyncio.StreamWriter
    ):
        self.reader = reader
        self.writer = writer
        self.id = addr2hostport(writer.get_extra_info('peername'))
        self.logger = logging.getLogger(self.id)
        self.__close_event = None

    @property
    def is_closing(self):
        return self.writer is None

    async def close(self):
        if self.writer is None:
            await self.__close_event()
        else:
            self.__close_event = asyncio.Event()
            writer = self.writer
            self.writer = None
            self.reader = None
            writer.close()
            try: # This may fail (BrokenPipeError)
                await writer.wait_closed()
            except Exception as err:
                self.logger.error(err)
            self.__close_event.set()
            self.logger.info("x")
    
    # Safe read from the channel
    async def __read(self, size: int, initial: bool = False) -> bytes:
        if self.reader is None: raise ChannelClosedError()
        try:
            return await self.reader.readexactly(size)
        except asyncio.IncompleteReadError as err:
            read = len(err.partial)
            if initial and read == 0:
                raise ChannelClosedError()
            else:
                raise ChannelIncompleteMessage(size, read)
        except ConnectionResetError:
            raise ChannelClosedError()

    async def recv(self) -> Tuple[str, str]:
        header = await self.__read(8, True)
        if header[0:4] != b'RWP1':
            raise ChannelProtocolError("invalid header")
        size = int.from_bytes(header[4:], 'little')
        message = (await self.__read(size)).decode().split(':',1)
        if len(message) < 2:
            self.logger.info(f"< {message[0]}")
            return message[0], None
        else:
            self.logger.info(f"< {message[0]} {message[1]}")
            return message

    async def send(self, command: str, data: str, slow=0):
        self.logger.info(f'> {data} ({command})')
        if self.is_closing:
            self.logger.warning('aleady closed')
            return
        encoded = f"{command}:{data}".encode()
        size = len(encoded)
        self.writer.write(b"RWP1")
        if slow: await asyncio.sleep(slow)
        self.writer.write(size.to_bytes(4, 'little'))
        if slow: await asyncio.sleep(slow)
        self.writer.write(encoded)
        try: # This may fail (BrokenPipeError)
            await self.writer.drain()
        except Exception as err:
            self.logger.exception(err)


class Server:
    def __init__(self, host: str, port: int):
        self.server = None
        self.host = host
        self.port = port
        self.channels: Set[Channel] = set()
        self.logger = logging.getLogger('server')

    async def run(self):
        server = asyncio.start_unix_server(self.handle_message, self.host) if self.port < 0 \
            else asyncio.start_server(self.handle_message, self.host, self.port)
        self.server = await server
        addrs = ', '.join(addr2hostport(sock.getsockname()) for sock in self.server.sockets)
        self.logger.info(f"serving on {addrs}")

        async with self.server:
            await self.server.serve_forever()

        self.server = None
        self.logger.info(f"stopped serving on {addrs}")


    async def handle_message(
        self,
        reader: asyncio.StreamReader,
        writer: asyncio.StreamWriter
    ):
        responses = {
            ACTION_GREET: "Hello, {}!",
            ACTION_BYE: "Bye, {}!",
            ACTION_NIGHT: "Good night, {}!",
            ACTION_FIRE: "{}, you're fired!",
        }
        channel = Channel(reader, writer)
        self.channels.add(channel)
        self.logger.info(f'serving {channel.id}')
        try:
            while True:
                command, data = await channel.recv()
                response = responses.get(command)
                if response:
                    await channel.send(command, response.format(data))
                else:
                    channel.logger.error(f"unknown command {command!r}")
                    await channel.send(command, f"I don't know that to do with {data}")
        except asyncio.CancelledError:
            channel.logger.warning('cancelled')
        except ChannelClosedError:
            pass
        except ChannelException as err:
            channel.logger.warning(err)
        except Exception as err:
            channel.logger.exception(err)
        finally:
            await channel.close()
            self.logger.info(f'closed {channel.id}')



if __name__ == "__main__":

    from utils import parse_host, run
    from sys import argv

    host, port = DEFAULT_HOST if len(argv) < 2 \
                 else parse_host(argv[1], default=DEFAULT_HOST)
    server = Server(host, port)
    run(server.run())

