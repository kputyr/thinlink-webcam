"""
WebRTC signaling channel test #2 (client)

Connects to a signaling server using a TCP socket and 
sends several messages at the same time. The messages
are encoded following a simple protocol that provides
the size of the encoded message.

For testing purposes the message can be sent in parts
with several second pauses, which allows to test how
the server behaves when a connection is lost before
an entire message is received.
"""

import asyncio
import logging
from typing import List, Optional

DEFAULT_HOST = ('127.0.0.1', 8888)

async def recv(reader: asyncio.StreamReader) -> str:
    try:
        header = await reader.readexactly(4)
        if header != b'RWP1': return None
        size = int.from_bytes(await reader.readexactly(4), 'little')
        return (await reader.readexactly(size)).decode()
    except Exception:
        return None

async def send(writer: asyncio.StreamWriter, message: str, slow=0):
    encoded = message.encode()
    size = len(encoded)
    writer.write(b"RWP1")
    if slow: await asyncio.sleep(slow)
    writer.write(size.to_bytes(4, 'little'))
    if slow: await asyncio.sleep(slow)
    writer.write(encoded)

async def make_request(host: str, port: int, names: List[str], slow: int):    
    logger = logging.getLogger(f'{host}:{port}')
    # Open a connection
    logger.info('opening')
    reader, writer = await asyncio.open_connection(host, port)
    # Send messages
    if len(names) == 0: names = ['World']
    for name in names:
        logger.info(f"< {name}")
        await send(writer, name, slow)
    await writer.drain()
    # Receive responses
    for name in names:
        data = await recv(reader)
        logger.info(f"> {data}")
    # Close the connection
    logger.info("closing")
    writer.close()
    await writer.wait_closed()


async def make_bad_request(host: str, port: int, header: Optional[str], truncated: int):
    logger = logging.getLogger(f'{host}:{port}')
    # Open a connection
    logger.info('opening')
    reader, writer = await asyncio.open_connection(host, port)
    # Prepare message metadata
    if header is None: header = 'RWP1'
    message = 'World'
    size = len(message)
    encoded = header.encode() + size.to_bytes(4, 'little') + message.encode()
    # Send messages
    if truncated > 0:
        logger.info(f"< {header}:{size}:{message} truncated after {truncated} bytes")
        writer.write(encoded[:truncated])
    else:
        logger.info(f"< {header}:{size}:{message}")
        writer.write(encoded)
    await writer.drain()
    writer.close()
    # Receive responses
    data = await recv(reader)
    logger.info(f"> {data}")
    # Close the connection
    logger.info("closing")
    writer.close()
    await writer.wait_closed()

if __name__ == "__main__":

    import argparse
    from utils import parse_host, create_logger

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "-a", "--addr",
        help=f"The address of the server in the format host:port, each component is optional. Default: {DEFAULT_HOST[0]}:{DEFAULT_HOST[1]}",
        type=str,
        required=False
    )
    parser.add_argument(
        "-s", "--slow",
        help="Slows down sending a message by inserting a pause in a few places",
        action="count",
        default=0
    )
    parser.add_argument(
        "-H", "--header",
        help="Use a custom header. This is good for testing protocol errors",
        required=False
    )
    parser.add_argument(
        "-t", "--truncated",
        help="Simulate a truncated message by passing a bigger length value",
        type=int,
        default=0,
        required=False
    )
    parser.add_argument(
        "name",
        help="Your name(s)",
        default=["World"],
        type=str,
        nargs='*'
    )
    args = parser.parse_args()
    host, port = parse_host(args.addr, default=DEFAULT_HOST)

    create_logger()
    if args.header or args.truncated:
        asyncio.run(make_bad_request(host, port, args.header, args.truncated))
    else:
        asyncio.run(make_request(host, port, args.name, args.slow))
