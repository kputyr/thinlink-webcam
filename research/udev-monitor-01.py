from pyudev import Context, Monitor, MonitorObserver, Device
from sys import stdin

def print_event(device: Device):
    print('{0.action}: {0.device_path}, {0.device_node}'.format(device))
    print(device)

context = Context()
monitor = Monitor.from_netlink(context)
monitor.filter_by('video4linux')

observer = MonitorObserver(monitor, callback=print_event)
observer.start()

print("Monitoring devices. Type 'q' to stop")
while stdin.read(1) != 'q':
    pass

observer.stop()
print("Monitoring has been stopped.")
