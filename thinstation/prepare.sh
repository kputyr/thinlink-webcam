#!/bin/sh

# This script prepares files for a ThinStation client
# It should be run from the directory with tswebcam folder in it

TARGET_DIR="./tswebcam/lib/tswebcam"
SOURCE_DIR="../src"
SSH_FILE="../opt/ssh"

[ -d tswebcam ] && [ -d tlclient ] || \
 { echo "Cannot find target directories. Please check if the script is started in the correct directory."; exit 1; }

[ -d $SOURCE_DIR ] && [ -f $SSH_FILE ] || \
 { echo "Cannot find source files. Please check if the script is started in the correct directory."; exit 1; }

echo "Copying Python files..."
\rm -rf $TARGET_DIR
for FILE in `cat files`; do
    DIR=`dirname $TARGET_DIR/$FILE`
    mkdir -p $DIR
    cp $SOURCE_DIR/$FILE $DIR
done

echo "Choose the constants file for ThinStation..."
mkdir -p $TARGET_DIR/constants
cp "$SOURCE_DIR/constants/thinstation.py" "$TARGET_DIR/constants/__init__.py"

echo "Copying the ssh wrapper script..."
\cp $SSH_FILE tlclient/

echo "Creating an archive file..."
tar -zcvf `date +"tswebcam-%Y%m%d.tar.gz"` tswebcam tlclient python3 install.sh dependencies

echo "Done"

