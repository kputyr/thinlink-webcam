#!/bin/sh

# The installation file is assumed to be run from $TS_DIR/build/packages/tswebcam/source

CWD=$PWD
SOURCE_DIR=`basename $PWD`
TSBUILD_PACKAGE_DIR=$(dirname `dirname $PWD`)

TLCLIENT_PACKAGE="thinlinc-my"
TSWEBCAM_PACKAGE="tswebcam"
PYTHON3_PACKAGE="python3"

TSWEBCAM_DIR="$TSBUILD_PACKAGE_DIR/$TSWEBCAM_PACKAGE"
TLCLIENT_DIR="$TSBUILD_PACKAGE_DIR/$TLCLIENT_PACKAGE/lib/tlclient"
PYTHON3_LIB="$TSBUILD_PACKAGE_DIR/$PYTHON3_PACKAGE/lib/python3"

# Check that dependencies are installed
[ -d $TLCLIENT_DIR ] && [ -d $PYTHON3_LIB ] || \
  { echo "Missing dependencies. Check if $TLCLIENT_PACKAGE and $PYTHON3_PACKAGE are properly installed."; exit 1; }

# Check if pip is available - it is in the the build environment, but not otherwise
which pip > /dev/null || { echo "Missing pip - are you in the build environment?"; exit 1; }

# Create a backup of old files
BACKUP_FILE=`date +"$TSBUILD_PACKAGE_DIR/$TSWEBCAM_PACKAGE-%y%m%s-%H%M%S.tar.gz"`
[ -d "$TSWEBCAM_DIR" ] && tar --exclude=$SOURCE_DIR -zcf $BACKUP_FILE $TSWEBCAM_DIR/*

# Install and patch python libraries
for MODULE in `cat dependencies`; do
    echo "Installing $MODULE..."
    pip install --target $PYTHON3_LIB $MODULE
done
echo "Applying patches to python modules..."
\cp -r python3/* $PYTHON3_LIB/

# Copy files and create shortcuts
echo "Installing tswebcam..."
for FILE in `ls`; do
    [ $FILE == $SOURCE_DIR ] || \rm -rf $TSWEBCAM_DIR/$FILE
done
cp -r tswebcam/* $TSWEBCAM_DIR

cd "$TSWEBCAM_DIR/bin"
ln -snf /lib/tswebcam/tswebcam-ctl.py tswebcam-ctl

cd "$TSWEBCAM_DIR/etc/systemd/system/multi-user.target.wants"
ln -snf ../tswebcam.service tswebcam.service

cd $CWD

# Hijack /lib/tlclient/ssh
echo "Updating tlclient..."
[ -f $TLCLIENT_DIR/ssh-tl ] || mv "$TLCLIENT_DIR/ssh" "$TLCLIENT_DIR/ssh-tl"
\cp "tlclient/ssh" "$TLCLIENT_DIR/ssh"

# That's it!
echo "Ready!"
