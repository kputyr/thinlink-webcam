from tkinter import *
from tkinter.ttk import *
from queue import SimpleQueue as Queue
from asyncio import get_event_loop, new_event_loop, run_coroutine_threadsafe, run, Event as AsyncEvent
from service.video_monitor import VideoDevicesMonitor, CameraInfo
from video.capture import CameraStreamTrack
from threading import Thread, Event
from typing import Optional, Callable

APP_TITLE = "ThinLinc webcam controller"
BTN_TEXT_STREAMING = "Stop streaming"
BTN_TEXT_NOT_STREAMING = "Start streaming"

CAMERA_SELECTOR_PROMPT = "-- choose a camera --"


class CameraPreview(Frame):
    def __init__(self, parent):
        super().__init__(parent, borderwidth=1)
    
    def display_video_frame(self, frame):
        pass


class CameraSelector(OptionMenu):
    def __init__(self, parent):
        self.__selection = StringVar(parent)
        self.__selected_id = None
        self.__ids = list()
        super().__init__(parent, self.__selection, CAMERA_SELECTOR_PROMPT)
        self.__menu: Menu = self['menu']

    @property
    def selected(self) -> Optional[str]:
        return self.__selected_id
    
    def __index_by_id(self, id: str) -> int:
        return next( (i for i,iid in enumerate(self.__ids) if id==iid), -1)

    def select(self, id: Optional[str]):
        print(f"SELECTOR: selecting {id}")
        if id is None:
            print(f"SELECTOR: selected None")
            self.__selected_id = None
            self.__selection.set(CAMERA_SELECTOR_PROMPT)
        else:
            index = self.__index_by_id(id)
            print(f"SELECTOR: selected {index}")
            if index < 0: return
            self.__selected_id = id
            self.__selection.set(self.__menu.entrycget(index, 'label'))
    
    def set_cameras(self, cameras):

        def selector(id):
            return lambda: self.select(id)

        self.__selection.set(CAMERA_SELECTOR_PROMPT)
        self.__menu.delete(0, 'end')
        self.__ids = [camera[0] for camera in cameras]
        print("SELECTOR ids: " + ", ".join(self.__ids))
        for id, label in cameras:
            print(f"SELECTOR: adding {id} - {label}")
            self.__menu.add_command(label=label, command=selector(id))

    def add_camera(self, id: str, label: str):
        self.__ids.append(id)
        self.__menu.add_command(label=label, command=lambda: self.select(id))

    def remove_camera(self, id: str):
        index = next( (i for i,iid in enumerate(self.__ids) if id==iid), -1)
        if index < 0: return
        if id == self.__selected_id:
            self.__selected_id = None
            self.__selection.set(CAMERA_SELECTOR_PROMPT)
        self.__ids.pop(index)
        self.__menu.delete(index, index)


class TLWebcam(Tk):
    def __init__(self, cameras):
        super().__init__()
        self.cameras = list()
        self.streaming = False
        self.title(APP_TITLE)
        self.resizable(False, False)
        self.create_widgets(cameras)

    def create_widgets(self, cameras):
        # Preview of the camera
        self.frm_preview = Frame(self, width=320, height=240, relief=GROOVE, borderwidth=3)
        self.frm_preview.grid(row=0, padx=5, pady=5)
        # Camera selector
        self.opt_camera = CameraSelector(self)
#        self.opt_camera.set_cameras(cameras)
        self.opt_camera.grid(row=1, pady=5)
        # Stream button
        self.btn_stream = Button(self, text=BTN_TEXT_NOT_STREAMING, command=self.on_button_clicked)
        self.btn_stream.grid(row=2, pady=5)
        # Last event
        self.last_event = Label(self, text="waiting...")
        self.last_event.grid(row=3, pady=10, padx=15)

    def monitor(self, events: Queue):
        try:
            event_name, *event_data = events.get_nowait()
            if event_name == '+cam':
                self.opt_camera.add_camera(*event_data)
            elif event_name == '-cam':
                self.opt_camera.remove_camera(*event_data)
            elif event_name == 'cams':
                self.opt_camera.set_cameras(event_data)
        except Exception:
            pass
        self.after(100, self.monitor, events)

    def on_button_clicked(self):
        self.streaming = not self.streaming
        self.btn_stream['text'] = BTN_TEXT_STREAMING if self.streaming else BTN_TEXT_NOT_STREAMING


class AsyncioThread(Thread):

    def __init__(self):
        self.__loop = None
        self.events = Queue()
        super().__init__(
            target=self.__run,
            name='asyncio-thread'
        )
        self.__stop = None

    def stop(self):
        async def set_stop():
            self.__stop.set()
        run_coroutine_threadsafe(set_stop(), self.__loop)

    def __run(self):
        self.__loop = new_event_loop()
        self.__loop.run_until_complete(self.__arun(self.events))

    async def __arun(self, queue: Queue):
        try:
            self.__stop = AsyncEvent()
            print("asyncio thread: creating a device monitor")
            self.monitor = VideoDevicesMonitor()

            @self.monitor.on(VideoDevicesMonitor.EV_CAMERA_ATTACHED)
            def on_attached(device: CameraInfo):
                queue.put(('+cam', device.id, device.label))

            @self.monitor.on(VideoDevicesMonitor.EV_CAMERA_DETACHED)
            def on_detached(device: CameraInfo):
                queue.put(('-cam', device.id))

            print("asyncio thread: starting the device monitor")
            await self.monitor.start()
            cams = [(camera.id, camera.label) for camera in self.monitor.cameras]
            print(cams)
            cams.insert(0, 'cams')
            queue.put(cams)
            print("asyncio thread: ready")
            await self.__stop.wait()
            print("asyncio thread: finishing")

        except BaseException as err:
            print(f"asyncio thread: error: {repr(err)}")
        finally:
            print("asyncio thread: stopping the device monitor")
            await self.monitor.stop()


if __name__ == "__main__":

    print("Staring asyncio thread")
    monitor = AsyncioThread()
    monitor.start()

    print("Creating TK app")
    app = TLWebcam(list())
    app.monitor(monitor.events)
    app.mainloop()

    print("Closing the asyncio thread")
    monitor.stop()
    monitor.join()
