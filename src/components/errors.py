"""
ThinLinc Webcam
author: Krzysztof Putyra
date: 29/03/2023

Definitions of application related exceptions.

These exceptions are logged as errors with no trace track
and they should be used to notify about problem that may
occur during a normal usage of the application. Problems
with code or logic must be notified with general exceptions
- these are logged with a trace track and a generic message
is shown to the user.
"""

class ApplicationError(Exception):
    message_format: str = "{}"
    code = 255

    def __str__(self):
        return self.message_format.format(*self.args)

class UnknownCommand(ApplicationError):
    message_format = "unknown command: {}"
    code = 16

class TokenLoadingError(ApplicationError):
    message_format = "failed to load a token: {}"
    code = 22

class AuthenticationError(ApplicationError):
    message_format = "authentication error: {}"
    code = 17
    
class ConnectionError(ApplicationError):
    message_format = "failed to connect to '{}': {}"
    code = 18

class ChannelClosedError(ApplicationError):
    message_format = "channel is closed"
    code = 19

class ProtocolError(ApplicationError):
    message_format = "protocol error: {}"
    code = 20

class UnknownUserError(ApplicationError):
    message_format = "user {} has no active session"
    code = 21

class RequestFailedError(ApplicationError):
    def __init__(self, code, msg):
        super().__init__(msg)
        self.code = code
