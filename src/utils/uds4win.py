import asyncio

if not hasattr(asyncio, 'open_unix_connection'):

    import struct
    from typing import Tuple, Callable

    SOCKET_DESCRIPTOR_HEADER = b'UDS4WIN'
    SOCKET_DESCRIPTOR_FORMAT = '@7sH'

    async def start_unix_server(
            callback: Callable,
            path: str,
            *args, **kwargs
    ) -> asyncio.AbstractServer:
        with open(path, "wb") as file:
            server = await asyncio.start_server(
                callback, 'localhost', None, *args, **kwargs
            )
            port = server.sockets[0].getsockname()[1]
            file.write(struct.pack(SOCKET_DESCRIPTOR_FORMAT, SOCKET_DESCRIPTOR_HEADER, port))
            return server

    async def open_unix_connection(
            path: str,
            *args, **kwargs
    ) -> Tuple[asyncio.StreamReader, asyncio.StreamWriter]:
        with open(path, "rb") as file:
            data = file.read()
        if len(data) != struct.calcsize(SOCKET_DESCRIPTOR_FORMAT):
            raise ConnectionRefusedError('Not a unix socket: ' + path)
        header, port = struct.unpack(SOCKET_DESCRIPTOR_FORMAT, data)
        if header != SOCKET_DESCRIPTOR_HEADER:
            raise ConnectionRefusedError('Not a unix socket: ' + path)
        return await asyncio.open_connection(
            'localhost', port, *args, **kwargs
        )
    
    setattr(asyncio, 'start_unix_server', start_unix_server)
    setattr(asyncio, 'open_unix_connection', open_unix_connection)


if __name__ == "__main__":

    import os
    
    async def main():

        async def send(w: asyncio.StreamWriter, msg: str):
            encoded = msg.encode()
            w.write(len(encoded).to_bytes(1, 'little'))
            w.write(encoded)
            await w.drain()

        async def recv(r: asyncio.StreamReader) -> str:
            size = await r.read(1)
            msg = await r.read(size[0])
            return msg.decode()

        async def onconnected(r: asyncio.StreamReader, w: asyncio.StreamWriter):
            print("Connected")
            msg = await recv(r)
            print("Got: " + msg)
            await send(w, "OK")
            w.close()
            await w.wait_closed()

        SOCKET_NAME = 'testunix'

        print("Opening a server")
        server = await asyncio.start_unix_server(onconnected, SOCKET_NAME)
        assert os.path.exists(SOCKET_NAME), "socket data file not found"
        server_task = asyncio.create_task(server.serve_forever())

        print("Opening a connection")
        client = await asyncio.open_unix_connection(SOCKET_NAME)
        print("Sending 'Hello'")
        await send(client[1], "Hello")
        resp = await recv(client[0])
        print("Got: " + resp)
        print("Closing the client")
        client[1].close()
        await client[1].wait_closed()
        print("Closing the server")
        server.close()
        try:
            await server_task
        except asyncio.CancelledError:
            pass
        print("Cleaning up")
        os.unlink(SOCKET_NAME)

    asyncio.run(main())
