from os.path import join

LOG_STREAM = False
LOG_LEVEL = 0
LOG_FILE = '/var/log/tlwebcam.log'

VIDEO_RUNNING_DIR = '/run/tswebcam'
VIDEO_CHANNEL = join(VIDEO_RUNNING_DIR, "channel")
VIDEO_AUTH_TOKEN = join(VIDEO_RUNNING_DIR, "authkey")

CLIENT_DATA_FILE = join(VIDEO_RUNNING_DIR, "storage")
