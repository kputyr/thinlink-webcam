from os.path import dirname, abspath, expanduser, join
from sys import stdout

LOG_STREAM = stdout
LOG_LEVEL = 0
LOG_FILE = "/var/log/tlwebcam.log"

#TLWEBCAM_MAIN_DIR = dirname(dirname(abspath(__file__)))
MAIN_DIR = dirname(dirname(abspath(__file__)))
USER_DIR = join(expanduser('~'), ".tlwebcam")

RUNNING_DIR = "/run/tlwebcam"
USER_RUNNING_DIR = "/home/{}/.tlwebcam"

SERVICE_CHANNEL = join(RUNNING_DIR, "channel")
"""The location of the unix socket for communication with the main daemon service"""

SERVICE_AUTH_TOKEN = join(RUNNING_DIR, "authkey")
"""The path to the file with the authentication key"""

USER_CHANNEL = join(USER_RUNNING_DIR, "channel")
"""The location of the unix socket for communication with the user daemon service"""

USER_AUTH_TOKEN = join(USER_RUNNING_DIR, "sessionkey")
"""The string pattern for the path to the session key"""


VIDEO_RUNNING_DIR = join(RUNNING_DIR, "camera")
VIDEO_CHANNEL = join(VIDEO_RUNNING_DIR, "channel")
VIDEO_AUTH_TOKEN = join(VIDEO_RUNNING_DIR, "authkey")

CLIENT_DATA_FILE = join(RUNNING_DIR, "storage")



V4L2LOOPBACK_CTL = "v4l2loopback-ctl"
SYSDEVICE_PATH = "/sys/devices/virtual/video4linux/"

THINLINC_PATH = "/opt/thinlinc/etc"
TL_HIVECONF_PATH = "/opt/thinlinc/etc/thinlinc.hconf"
