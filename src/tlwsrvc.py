"""
ThinLink Webcam main service

app.add_service(service)
app.process_requests({
   request: handler
}).using(addr, admin_token, user_token)

app.listen_on(addr, token, token).for_requests({

})

"""
# TODO: monitor loopback device for usage
# Use Inotify for events
# Warning: events may get lost!

import secrets
import struct

from app import ServiceApp, make_request

from components.errors import AuthenticationError, RequestFailedError
from components.session import Session

from service.loopback import LoopbackService, LoopbackDevice
from utils import load_token

import commands as cmd
from constants import RUNNING_DIR, SERVICE_CHANNEL, SERVICE_AUTH_TOKEN, \
    USER_AUTH_TOKEN, USER_CHANNEL

app = ServiceApp('tlwsrvc', running_dir=RUNNING_DIR, token_path=SERVICE_AUTH_TOKEN)
loopback = LoopbackService(name='monitor')

@loopback.on(LoopbackService.EV_STREAM_ON)
async def stream_on(device: LoopbackDevice):
    try:
        await make_request(USER_CHANNEL.format(device.owner), cmd.STREAM, b'')
    except Exception:
        pass

@loopback.on(LoopbackService.EV_STREAM_OFF)
async def stream_off(device: LoopbackDevice):
    try:
        await make_request(USER_CHANNEL.format(device.owner), cmd.STOP, b'')
    except Exception:
        pass


def authenticate(session: Session, username: bytes):
    if session['is_admin'] or session['authenticated'] == username: return
    if session['authenticated'] is None and username != b'':
        try:
            user_token = load_token(
                USER_AUTH_TOKEN.format(username.decode())
            )
        except FileNotFoundError as err:
            raise AuthenticationError(f'token {err.filename} not found')
        except PermissionError as err:
            raise AuthenticationError(f'access denied to {err.filename}')

        if secrets.compare_digest(user_token, session['auth_token']):
            session['authenticated'] = username
            return

    raise AuthenticationError('invalid token')

async def accept_token(session: Session, token: bytes):
    is_admin = secrets.compare_digest(token, app.secret_token)
    session['is_admin'] = is_admin
    session['auth_token'] = token
    session['authenticated'] = None

async def create_device(session: Session, username: bytes) -> bytes:
    authenticate(session, username)
    device = await loopback.get_device(username.decode())
    if device:
        return struct.pack("=B", device.index)
    else:
        raise RequestFailedError(10, "failed to create a loopback device")

async def delete_device(session: Session, username: bytes) -> bytes:
    authenticate(session, username)
    device = await loopback.remove_device(username.decode())
    return b'' if device is None else struct.pack("=B", device.index)

async def list_devices(session: Session, username: bytes):
    authenticate(session, username)
    if session['is_admin']:
        devices = ",".join((
            f"{device.index}:{device.owner}" for device in loopback.devices
        ))
    else:
        devices = ",".join(
            str(device.index) for device in loopback.get_devices_by_owner(username.decode())
        )
    return devices.encode()

async def unload_service(session: Session, data: bytes):
    if not session['is_admin']: raise AuthenticationError()
    app.stop()
    return b''

async def update_service(session: Session, data: bytes) -> bytes:
    added, removed = await loopback.update()
    exists = len(loopback.devices)
    return bytearray((exists,added,removed))

app.add_controller(SERVICE_CHANNEL, handlers={
    cmd.AUTHENTICATE  : accept_token,
    cmd.GET_DEVICE : create_device,
    cmd.DELETE_DEVICE : delete_device,
    cmd.LIST_DEVICES : list_devices,
    cmd.UNLOAD : unload_service,
    cmd.UPDATE : update_service
})
app.services.add(loopback)
app.run()
