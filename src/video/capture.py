import asyncio
import threading
import logging
import errno
import time
from typing import Optional, Any, Union, Coroutine, Set
from queue import Queue

from service.base import ActiveService
from service.video_monitor import CameraCollection, CameraInfo

from av import Packet, VideoFrame, FFmpegError, open as av_open
from av.container import InputContainer
from aiortc.mediastreams import MediaStreamError, MediaStreamTrack

DEFAULT_VIDEO_WIDTH = 640
DEFAULT_VIDEO_HEIGHT = 480
FRAME_QUEUE_SIZE = 5

# Notes on implementation
#
# Frames are captured and decoded in a separate thread that runs the function
# decode_video_source(). The frames are exchanged between this and the main
# thread with a queue. For some reason the last frame stays available after
# a source is closed and accessing it raises segmantation fault. In order to
# avoid this, a frame is put on a queue together with a source id that is
# incremented each time a source is switched. The method recv() then reads
# frames from the queue until the source id matches the current one.

log = logging.getLogger('video-capture')
log_opener = log.getChild(f'opener')
log_decoder = log.getChild(f'decoder')

DECODER_EVENT_SOURCE = 0
DECODER_EVENT_QUIT = 1

def open_video_source(
        path: str,
        width: int,
        height: int,
        input_format: str = 'mjpeg',
        framerate: Any = None
) -> InputContainer:

    container = None
    try:
        # Open the camera for streaming
        log_opener.info(f"{path}: opening")
        options = {
            "input_format": input_format,
            "video_size": f"{width}x{height}"
        }
        if framerate is not None: options['framerate'] = str(framerate)
        container = av_open(
            path,
            mode='r',
            format='v4l2',
            options=options
        )
        log_opener.debug(f"{path}: checking format")
        # Check the size of the stream 
        # V4L2 may change the format internally, so that we cannot
        # depend on stream.width and stream.height
        frame = next(container.decode(container.streams.video[0]))
        if frame.width != width or frame.height != height:
            raise RuntimeError('invalid video format')
        
        log_opener.debug(f"{path}: opened")
        return container

    except BaseException as err:
        log_opener.warning(f"{path} input error: {err}")
        if container: container.close()
        raise


class DropFrameMonitor:
    def __init__(self, logger: logging.Logger, interval: int = 30):
        self.logger = logger
        self.dropped = 0
        self.recently_dropped = 0
        self.next_warning_time = 0
        self.warning_interval = interval
    def reset(self):
        self.log_stats()
        self.dropped = 0
        self.recently_dropped = 0
        self.start_time = time.time()
        self.next_warning_time = self.start_time + self.warning_interval
    def mark(self):
        self.recently_dropped += 1
        cur_time = time.time()
        if next_drop_warning_time < cur_time:
            self.dropped += self.recently_dropped
            self.logger.warning(f"{self.recently_dropped} frames dropped ({self.dropped} in total)")
            self.recently_dropped = 0
            next_drop_warning_time = cur_time + 30
    def log_stats(self):
        self.dropped += self.recently_dropped
        if self.dropped == 0: return
        elapsed_time = time.time() - self.start_time
        if elapsed_time < 1: return
        drop_rate = self.dropped / elapsed_time
        self.logger.info(f"{self.dropped} frames dropped, {drop_rate:.2f} per second on average")


def decode_video_source(
        target: asyncio.Queue,
        events: Queue,
        loop: asyncio.AbstractEventLoop
):

    log_decoder.info('starts the decoding thread')

    source = None
    source_id = 0
    start_pts = 0

    drop_monitor = DropFrameMonitor(logger=log_decoder)

    while not loop.is_closed():
        
        # wait for a source if none
        log_decoder.info(f"waiting for a source")
        while source is None:
            event, *data = events.get()
            # Parse the event
            if event == DECODER_EVENT_QUIT:
                log_decoder.info(f'stops the decoding thread')
                return
            # event == DECODER_EVENT_SOURCE
            source = data[0]

        # At this moment source is not empty
        source_id += 1
        log_decoder.info(f"event: set source to {source.name} (#{source_id})")
        assert source.streams != None
        assert len(source.streams.video) == 1

        stream = source.streams.video[0]
        forward = stream.codec_context.name == 'h264'
        frames = source.demux(stream) if forward else source.decode(stream)
        # do not keep a reference
        stream = None

        first_frame = True
        log_decoder.debug(f'parsed update #{source_id}')

        # At this moment source is not empty, so that we can read frames

        if forward:
            log_decoder.info(f"demuxing {source.name}")
        else:
            log_decoder.info(f"decoding {source.name}")
        while events.empty() and not loop.is_closed():
            try:
                if first_frame:
                    log_decoder.debug(f"reading the first frame")
                    drop_monitor.reset()
                frame = next(frames)
                if frame.pts is None:
                    log_decoder.debug(f"skipping a frame/packet with no pts")
                    continue
                if first_frame:
                    if forward:
                        log_decoder.debug("format: encoded")
                    else:
                        log_decoder.debug(f"format: {frame.width}x{frame.height} ({frame.format.name})")
                    first_frame = False
                    start_pts = frame.pts - start_pts
                if target.qsize() < FRAME_QUEUE_SIZE:
                    frame.pts -= start_pts
                    asyncio.run_coroutine_threadsafe(target.put((source_id, frame)), loop)
                else:
                    drop_monitor.mark()

            except Exception as err:
                if isinstance(err, FFmpegError) and err.errno == errno.EAGAIN:
                    log.warning(str(err))
                    time.sleep(0.01)
                    continue
                else:
                    log_decoder.error(f"failed to process a video frame: {err}")
                    break

        # There is an event. Prepare by closing the current source
        log_decoder.info(f"closing {source.name}")
        log_decoder.debug(f"frames left: {target.qsize()}")
        drop_monitor.log_stats()

        frames.close()
        del frames
        source.close()
        source = None


class CameraStreamTrack(MediaStreamTrack):

    kind = "video"

    def __init__(
            self,
            width: int,
            height: int,
            input_format: str = 'mjpeg'
    ):
        super().__init__()
        self.__width = width
        self.__height = height
        self.__input_format = input_format
        self.__loop = asyncio.get_event_loop()
        self.__frames = asyncio.Queue(FRAME_QUEUE_SIZE)
        self.__thread_events = Queue()
        self.__decoder = threading.Thread(
            target=decode_video_source,
            name='video-decoder',
            args=(self.__frames, self.__thread_events, self.__loop),
            daemon=True
        )
        self.__source: Optional[InputContainer] = None
        self.__source_id = 0
        self.__decoder.start()

    @property
    def width(self) -> int:
        return self.__width
    
    @property
    def height(self) -> int:
        return self.__height

    @property
    def source(self) -> Optional[str]:
        return self.__source and self.__source.name

    def stop(self):
        self.__thread_events.put((DECODER_EVENT_QUIT,))
        super().stop()
        self.__decoder.join()

    async def set_source(self, path: Optional[str] = None, framerate: Any = None):
        log.debug(f'setting source to {path}')
        if path == self.source:
            log.debug('no change in source - return')
            return
        elif path is None:
            log.debug('closing source')
            self.__source = None
            self.__thread_events.put((DECODER_EVENT_SOURCE, None))
            pass
        else:
            log.debug(f'calling opener for {path}')
            source = await self.__loop.run_in_executor(
                None, open_video_source,
                path, self.width, self.height, self.__input_format, framerate
            )
            log.debug(f'opener returned {source}')
            self.__source = source
            self.__source_id += 1
            self.__thread_events.put((DECODER_EVENT_SOURCE, source))

    async def recv(self) -> VideoFrame:
        while True:
            source_id, frame = await self.__frames.get()
            if self.__source is not None and source_id == self.__source_id:
                return frame

