import logging
from fractions import Fraction
from typing import Tuple, NamedTuple, Dict, List
from hashlib import md5
from base64 import urlsafe_b64encode
from struct import pack

import v4l2py.device as v4l2
from v4l2py.io import fopen as v4l2open


# log_ioctl and log_mmap are bugged
from v4l2py.device import log_ioctl, log_mmap
log_ioctl.setLevel(logging.INFO)
log_mmap.setLevel(logging.INFO)


def encode_fraction(fraction: Fraction) -> str:
    return str(fraction.numerator) if fraction.denominator == 1 \
                else f"{fraction.numerator}/{fraction.denominator}"

def decode_fraction(fraction: str) -> Fraction:
    parts = [int(x) for x in fraction.split('/', 1)]
    return Fraction(parts[0], 1 if len(parts) == 0 else parts[1])


def includes_frame_size(sizes, width: int, height: int):
    for format in sizes:
        if width and format['width'] != width: continue
        if height and format['height'] != height: continue
        return True
    return False


class CameraInfo(NamedTuple):
    """
    A readonly collection of properties of a camera:
    - `id: str`: a unique ID of the camera
    - `path: str`: a path to the device file
    - `label: str`: the name of the camera
    - `formats: dict`: frame formats supported by the camera grouped by pixel format
    """

    # device.open() is bugged: if raises an OSError unless
    # the video device can be captured. Therefore we read
    # the properties directly

    id: str
    path: str
    label: str
    driver: str
    version: str
    bus_info: str
    capabilities: v4l2.Capability
    device_capabilities: v4l2.Capability
    formats: Dict[str, list]

    def is_match(self, id: str) -> bool:
        return self.id[:10] == id[:10]
    
    def supports(
            self,
            width: int = 0,
            height: int = 0, *,
            pixel_format: str = None
    ) -> bool:
        if pixel_format:
            formats = self.formats.get(pixel_format)
            return formats and includes_frame_size(formats, width, height)
        elif width == 0 and height == 0:
            return True
        else:
            return next(
                (True for format in self.formats.values() \
                    if includes_frame_size(format, width, height)),
                False
            )
    
    def get_closest_formats(self, width: int, height: int):

        def compute_parameters(fwidth: int, fheight: int):
            ratio = Fraction(fwidth, fheight)
            if ratio > ideal_ratio:   # wider
                scale = Fraction(height, fheight)
            else:
                scale = Fraction(width, fwidth)
            
            if scale == 1:
                scale_rate = 0
            elif scale < 1:
                scale_rate = 1
                scale = 1 / scale
            else:
                scale_rate = 2

            if ratio == ideal_ratio:
                ratio_rate = 0
                ratio_value = 1
            elif ratio < ideal_ratio:   # thinner
                ratio_rate = 1
                ratio_value = ideal_ratio / ratio
            else:
                ratio_rate = 2
                ratio_value = ratio / ideal_ratio

            return scale_rate, ratio_rate, ratio_value, scale
        
        def compare(a, b) -> int:
            # scale_rate, ratio_rate, scale_value, ratio_value
            # lexicographic order, the lower the better
            for x,y in zip(a,b):
                if x < y: return -1
                if x > y: return  1
            return 0


        result = {}
        ideal_ratio = Fraction(width, height)
        for pixel_format, formats in self.formats.items():
            best_found = None
            for format in formats:
                if format['width'] == width and format['height'] == height:
                    best_found = format
                    break
                elif best_found is None:
                    best_found = format
                    best_params = compute_parameters(format['width'], format['height'])
                else:
                    params = compute_parameters(format['width'], format['height'])
                    if compare(params, best_params) < 0:
                        best_found = format
                        best_params = params
            result[pixel_format] = best_found
        return result


    def pixel_formats(self, width: int = 0, height: int = 0) -> Tuple[str]:
        if width == 0 and height == 0: return tuple(self.formats)
        return tuple(
            pix_fmt for pix_fmt, sizes in self.formats.items() \
                if includes_frame_size(sizes, width, height)
        )
    
    @classmethod
    def from_path(cls, path: str):
        with v4l2open(path, True) as fd:
            caps = v4l2.read_capabilities(fd)
            device_capabilities = v4l2.Capability(caps.device_caps)
            if v4l2.Capability.VIDEO_CAPTURE not in device_capabilities:
                return None

            version_str = ".".join(map(str, (
                (caps.version & 0xFF0000) >> 16,
                (caps.version & 0x00FF00) >> 8,
                (caps.version & 0x0000FF),
            )))

            img_fmt_stream_types = {
                v4l2.BufferType.VIDEO_CAPTURE,
                v4l2.BufferType.VIDEO_CAPTURE_MPLANE,
                v4l2.BufferType.VIDEO_OUTPUT,
                v4l2.BufferType.VIDEO_OUTPUT_MPLANE,
                v4l2.BufferType.VIDEO_OVERLAY,
            } & set(typ for typ in v4l2.BufferType \
                    if v4l2.Capability[typ.name] in device_capabilities)

            image_formats = []
            pixel_formats = set()
            for stream_type in img_fmt_stream_types:
                for image_format in v4l2.iter_read_formats(fd, stream_type):
                    image_formats.append(image_format)
                    pixel_formats.add(image_format.pixel_format)

            # Supported formats groups by pixel format and then frame size
            # Example:
            # - YU12:
            #     - width: 1920
            #       height: 1080
            #       fps: [30, 25, 20, 15, 10, 5]
            #     - width: 1024
            #       height: 728
            #       fps: [30, 25, 20, 15, 10, 5]
            # - H264:
            #     - width: 1920
            #       height: 1080
            #       fps: [30, 25, 20, 15, 10, 5]
            #
            # The list of fps values is first computed as a set and then
            # converted to a tuple
            #
            formats: Dict[str, list] = dict()
            # A temporary dictionary with a flat structure
            format_by_id = dict()
            for frame in v4l2.frame_sizes(fd, pixel_formats):
                pix_fmt = frame.pixel_format.name
                sizes = formats.get(pix_fmt)
                if sizes is None:
                    sizes = list()
                    formats[pix_fmt] = sizes
                fmt_id = (frame.width, frame.height, pix_fmt)

                format = format_by_id.get(fmt_id)
                if format is None:
                    format = {
                        "width": frame.width,
                        "height": frame.height,
                        "fps": set()
                    }
                    format_by_id[fmt_id] = format
                    sizes.append(format)
                    fps = frame.min_fps
                    while True:
                        format['fps'].add(fps)
                        if fps >= frame.max_fps: break
                        fps += frame.step_fps
            # Now replace sets with tuples
            for format in format_by_id.values():
                format['fps'] = tuple(encode_fraction(fps) for fps in format['fps'])

            # Compute a hash using stable data:
            # - card name
            # - driver name
            # - camera capabilities
            hash = md5()
            hash.update(caps.card)
            hash.update(caps.driver)
            hash.update(pack("@II", caps.capabilities, caps.device_caps))
            # in order to make id unique, the device index is appended
            id = urlsafe_b64encode(hash.digest())[:10].decode() + path[10:].rjust(2, '0')

            return cls(
                id=id,
                path=path,
                label=caps.card.decode(),
                driver=caps.driver.decode(),
                version=version_str,
                bus_info=caps.bus_info.decode(),
                capabilities=v4l2.Capability(caps.capabilities),
                device_capabilities=device_capabilities,
                formats=formats
            )
