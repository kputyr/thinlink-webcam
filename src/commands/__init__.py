AUTHENTICATE = b'auth'
GET_DEVICE = b'vget'
DELETE_DEVICE = b'vdel'
LIST_DEVICES = b'list'
UPDATE = b'upde'
UNLOAD = b'quit'
CHANNEL = b'tsch'

STREAM = b'strm'
STOP = b'stop'
STATUS = b'stat'
LIST_CAMERAS = b'enum'
LIST_FORMATS = b'fmts'

CONNECT = b'conn'
DISCONNECT = b'disc'

TAKE_ANSWER = b'answ'
TAKE_OFFER = b'offr'

CAMERA_ATTACHED = b'+cam'
CAMERA_DETACHED = b'-cam'
