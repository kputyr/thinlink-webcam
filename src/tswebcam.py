"""
ThinStation Webcam main service

A list of services
``````````````````
UnixServer:
    Listens for local requests and processes them. Handlers of the requests
    require access to other services.
    - UNLOAD: stops all services and unloads the program
    - STOP: asks StreamSender to stop streaming 
    - STATUS: returns the status of StreamSender
    - LIST_CAMERAS: returns the cameras provided by VideoDeviceMonitor
    - LIST_FORMATS(id): returns formats provided by a camera
    - PREPARE_CHANNEL: prepares a channel and returns an authentication data

VideoDeviceMonitor:
    Provides a current list of available cameras and monitors changes.
    Cameras are identified by unique IDs and can be opened to access
    a video track usable for streaming.
    - triggers on_camera_attached when a new camera is detected
    - triggers on_camera_detached when a camera is removed

StreamSender:
    Controls a WebRTC stream. Requires a signaling channel to initiate
    a stream. Once the stream is established, the channel is no longer
    used and can be closed. However, in a future we may reuse the channel
    to renegotiate the connection.
    - start(video_track, channel) initiates a stream using the provided channel
      This requires to implement handlers for the following commands:
      - TAKE_ANSWER
      - ICE_CANDIDATE
    - stop() stops streaming

IceCandidateGatherer
    Reads ICE candidates received on a signaling channel and returns a list.
    This service can be implemented inside StreamReader, because its usage
    is localized to initiating a single stream.

ThinChannel:
    A service listening for requests from the ThinSession counterpart.
    - LIST_CAMERAS
    - LIST_FORMATS
    - STATUS
    - STOP: asks StreamSender to stop the stream
    - START: asks StreamSender to start a stream
    - SELECT_CAMERA(cam_id): selects the camera for streaming

"""

import secrets
import json
import logging

from app import ServiceApp

from components.errors import AuthenticationError, RequestFailedError
from components.session import Session

from service.video_monitor import VideoDevicesMonitor, CameraInfo
from service.video_capture import VideoInput
from service.rtcsender import StreamProvider

from components.storage import ClientStorage

import commands as cmd
from constants import VIDEO_CHANNEL, VIDEO_RUNNING_DIR, CLIENT_DATA_FILE

# These parameters should be saved in a config file
CAMERA_WIDTH = 640
CAMERA_HEIGHT = 480
CAMERA_FRAMERATE = 15

logging.getLogger('aiortc').setLevel(logging.INFO)
logging.getLogger('aioice').setLevel(logging.INFO)

app = ServiceApp('tswebcam', running_dir=VIDEO_RUNNING_DIR)
video = VideoDevicesMonitor(name='monitor')
video_input = VideoInput(
    name='video',
    width = CAMERA_WIDTH,
    height = CAMERA_HEIGHT,
    framerate = CAMERA_FRAMERATE
)
stream_provider = StreamProvider(
    name='webrtc',
    capture=video_input.capture
)

@app.on(app.EV_READY)
async def connect():
    app.logger.debug(f'connecting to open thin linc sessions')
    for port in ClientStorage(CLIENT_DATA_FILE):
        stream_provider.connect(port)

app.requested_source = ''     # None means default

@video.on(video.EV_CAMERA_ATTACHED)
async def on_cam_attached(camera: CameraInfo):
    try:
        await stream_provider.send(cmd.CAMERA_ATTACHED, camera.id.encode())
    except Exception as err:
        app.logger.error(f'failed to send CAMERA_ATTACHED: {err!r}')

    if stream_provider.is_streaming:
        if video_input.source:  # if none, then we try to open the camera anyway
            if app.requested_source:
                if video_input.source.is_match(app.requested_source):
                    # already the best match
                    return
                camera = video.cameras.best_match(app.requested_source, video_input)
            else:
                camera = video.cameras.default(video_input.width, video_input.height)
            if camera == video_input.source: return
        try:
            app.logger.info(f'updating the stream source to {camera.path} (default)')
            await video_input.open(camera)
        except Exception as err:
            app.logger.error(f'failed to change the default camera to {camera.path}')


@video.on(video.EV_CAMERA_DETACHED)
async def on_cam_detached(camera: CameraInfo):
    try:
        await stream_provider.send(cmd.CAMERA_DETACHED, camera.id.encode())
    except Exception as err:
        app.logger.error(f'failed to send CAMERA_DETACHED: {err!r}')
    if video_input.source == camera:
        app.logger.info(f'the currently captured camera {camera.path} has been detached - switching to a default one')
        try:
            new_source = video.cameras.default(video_input.width, video_input.height)
            if new_source is None:
                app.logger.warning(f'no compatible camera found - stopping the stream')
                # This will close video_input
                await stream_provider.stream_off()
            else:
                app.logger.info(f'switching the input camera to {new_source.path}')
                await video_input.open(new_source)
        except Exception as err:
            app.logger.error(f'failed to switch the input to {camera.path} - stopping the stream')
            await stream_provider.stream_off()
    



def authenticate(session: Session):
    if not session['authenticated']:
        raise AuthenticationError('bad token')

async def accept_token(session: Session, token: bytes):
    session['authenticated'] = secrets.compare_digest(app.secret_token, token)

async def unload_service(session: Session, data: bytes):
#    authenticate(session)
    app.stop()
    return b''

# TODO: implement requesting a different frame format
async def start_streaming(session: Session, cam_id: bytes) -> bytes:
    # Translate cam_id to a CameraInfo object
    cam_id = cam_id.decode()
    if cam_id == '':
        camera = video.cameras.default(video_input.width, video_input.height)
        app.logger.debug(f'requested the default camera and found {camera and camera.path}')
    else:
        camera = video.cameras.best_match(cam_id, video_input.width, video_input.height)
        app.logger.debug(f'requested camera with id {cam_id} and found {camera and camera.path}')
    if camera is None:
        raise RequestFailedError(40, 'no compatible camera found')
    app.requested_source = cam_id
    # Open the source with the current settings. On success update
    # current streaming settings and start the stream unless already active
    try:
        await video_input.open(camera)
        if not stream_provider.is_streaming:
            await stream_provider.stream_on()
        return camera.id.encode()
    except Exception:
        raise RequestFailedError(100, f"failed to open the camera")

async def stop_streaming(session: Session, data: bytes):
    await stream_provider.stream_off()
    video_input.close()
    return b''

async def fetch_status(session: Session, data: bytes):
    return json.dumps({
        "sessions": 0,
        "connected": 0,
        "streaming": False
    }).encode()

async def list_cameras(session: Session, data: bytes) -> bytes:
    return json.dumps(
        [(camera.id, camera.label) for camera in video.cameras]
    ).encode() if len(video.cameras) else b''

async def list_formats(session: Session, data: bytes) -> bytes:
    camera = video.cameras.get_id(data.decode())
    if camera is None:
        raise RequestFailedError(100, 'camera not found')
    return json.dumps(camera.formats).encode()


async def connect_channel(session: Session, data: bytes) -> bytes:
    stream_provider.connect(data and int(data.decode()))
    return b''

async def disconnect_channel(session: Session, data: bytes) -> bytes:
    stream_provider.disconnect(data and int(data.decode()))
    return b''


stream_provider.set_handlers({
    cmd.LIST_CAMERAS: list_cameras,
    cmd.LIST_FORMATS: list_formats,
    cmd.STREAM: start_streaming,
    cmd.STOP: stop_streaming
})

app.services.add(stream_provider)
app.services.add(video)
app.services.add(video_input)
app.add_controller(VIDEO_CHANNEL, handlers={
    cmd.AUTHENTICATE  : accept_token,
    cmd.UNLOAD : unload_service,
    cmd.STOP: stop_streaming,
    cmd.STATUS: fetch_status,
    cmd.LIST_CAMERAS: list_cameras,
    cmd.LIST_FORMATS: list_formats,
    cmd.CONNECT: connect_channel,
    cmd.DISCONNECT: disconnect_channel
})

app.run()
