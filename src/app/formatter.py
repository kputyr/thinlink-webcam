from typing import Dict, List, Any

def device_list(data: bytes):
    devices = [line.split(':') for line in data.decode().split(',')]
    if len(devices) > 0 and len(devices[0]) == 1:
        # Only devices of the current user
        return "\n".join(f"/dev/video{device[0]}" for device in devices)
    else:
        return "\n".join(
            "/dev/video" + device[0].ljust(5) + device[1] for device in devices
        )

def packed(format: str, *prompts):
    import struct
    
    def formatter(data: bytes):
        prompt_space = max(len(prompt) for prompt in prompts) + 2
        return "\n".join((
            prompt + ':'.ljust(prompt_space - len(prompt)) + str(number) \
            for prompt, number in zip(prompts, struct.unpack(format, data))
        ))

    return formatter

def json_table(data: bytes) -> str:
    import json
    rows: List[Dict[str, Any]] = json.loads(data)
    if len(rows) == 0: return ""
    first_row = rows[0]
    sizes: Dict[str,int] = {
        field: max(len(field), max(len(str(row[field])) for row in rows)) \
            for field in first_row.keys()
    }
    row_meta = [
        (field, size, str.rjust if isinstance(first_row[field], (int, float)) \
                else str.ljust) for field, size in sizes.items()
    ]
    hline = "  ".join(("".ljust(size, "-") for size in sizes.values()))
    return (
        "  ".join( field.ljust(size) for field,size in sizes.items() ) +
        f"\n{hline}\n" +
        "\n".join((
            "  ".join( (padder(str(row[field]), size) for field, size, padder in row_meta) ) \
                for row in rows
        ))
    )

def json_object(data: bytes) -> str:
    import json
    return json.dumps(
        json.loads(data),
        indent=4,
        check_circular=False,
        separators=('',': ')
    )

def camera_list(data: bytes) -> str:
    import json
    cameras = json.loads(data)
    if len(cameras) == 0: return "no camera available"
    col_id_width = max(9, max(len(cam[0]) for cam in cameras))
    col_label_width = max(5, max(len(cam[1]) for cam in cameras))
    headers = " id  " + "camera id".ljust(col_id_width) + "  label\n"
    separators = "---  " + "".ljust(col_id_width, "-") + "  ".ljust(col_label_width, "-") + "\n"
    return (
        headers + separators + "\n".join((
            str(id+1).rjust(3) + "  " + cam[0].ljust(col_id_width+2) + cam[1] for id, cam in enumerate(cameras)
        )) + "\n"
    )

def format_list(data: bytes) -> str:
    import json
    lines = []
    for format, sizes in json.loads(data).items():
        lines.append(format + ":")
        for size in sizes:
            lines.append("{:>8}x{}@{}".format(size['width'], size['height'], ",".join(size['fps'])))
    return "\n".join(lines)



if __name__ == "__main__":
    import json

    print("\nTEST: json_object")
    data = json.dumps({
        "title": "Test data",
        "date": "yesterday or tomorrow",
        "number": 42,
        "colors": ["red","green","blue"]
    }).encode()
    print(json_object(data))

    print("\nTEST: json_table")
    data = json.dumps([
        {"id": 240, "long": 2, "name": "Terminal"},
        {"id": 42, "long": 3, "name": "Hitchhiker"}, 
        {"id": 18, "long": 5, "name": "Whatever suitable"}
    ], separators=(',',':')).encode()
    print(json_table(data))

    print("\nTEST: camera_list")
    data = json.dumps([
        ("abcdefgh", "First camera"),
        ("FHAJS", "Second camera"),
        ("321bc23gh", "A camera with a very long name")
    ]).encode()
    print(camera_list(data))

    print("\nTEST: camera_list (short values)")
    data = json.dumps([
        ("abc", "A"),
        ("FH", "B"),
        ("32", "C")
    ]).encode()
    print(camera_list(data))
