import asyncio
import secrets
import logging
import signal
from pathlib import Path
from typing import Set, Dict, Optional
from pyee.asyncio import AsyncIOEventEmitter

from service.base import Service
from service.server import UnixServer, RequestHandler
from utils import save_token, hostname
from components import create_logger

class ServiceApp(AsyncIOEventEmitter):

    EV_READY = 'ready'

    def __init__(
            self,
            name: str,
            /,
            running_dir: str = None,
            token_path: Optional[str] = None,
            username: Optional[str] = None
    ):
        create_logger(
            appname=name,
            hostname=hostname(),
            username=username
        )
        super().__init__()
        self.name = name
        self.logger = logging.getLogger('app')
        self.services: Set[Service] = set()
        self.__stop_event = None
        if running_dir:
            # ensure that the running folder exists
            Path(running_dir).mkdir(parents=True, exist_ok=True)
        if token_path:
            try:
                self.secret_token = secrets.token_bytes(32)
                # ensure that the folder for the token exists
                if token_path != running_dir:
                    Path(running_dir).mkdir(parents=True, exist_ok=True)
                save_token(token_path, self.secret_token)
            except Exception as err:
                self.logger.exception(err)
                raise
        else:
            self.secret_token = None


    def stop(self):
        self.__stop_event.set()

    def run(self):

        def handle_signal(signum, frame):
            self.logger.warning(f"Received SIG {signum} - terminating the service")
            self.stop()
#            loop.run_until_complete(self.tear_down())
            exit(-signum)

        for signum in (signal.SIGINT, signal.SIGTERM, signal.SIGTSTP):
            signal.signal(signum, handle_signal)

        loop = asyncio.new_event_loop()
        try:
            loop.run_until_complete(self.set_up())
            loop.run_until_complete(self.__stop_event.wait())
        finally:
            loop.run_until_complete(self.tear_down())

    async def set_up(self):
        self.__stop_event = asyncio.Event()
        await asyncio.gather(*(service.start() for service in self.services))
        self.emit(self.EV_READY)

    async def tear_down(self):
        await asyncio.gather(*(service.stop() for service in self.services))

    def add_controller(
            self,
            path: str, /,
            handlers: Dict[bytes, RequestHandler],
            private: bool = False
    ):
        controller = UnixServer(
            path,
            mode=UnixServer.PRIVATE_MODE if private else UnixServer.PUBLIC_MODE,
            name='controller'
        )
        controller.set_handlers(handlers)
        self.services.add(controller)
