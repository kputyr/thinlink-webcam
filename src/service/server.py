import asyncio
import os
from stat import S_IRWXG, S_IRWXU, S_IRWXO
from typing import Optional, Callable, Dict, Set
from logging import Logger

from .base import ActiveService
from components import Channel
from components.errors import ApplicationError
from components.session import Session, SimpleServerSession, RequestHandler


class AbstractServer(ActiveService):

    EV_CONNECTION = 'connection'

    def __init__(
            self,
            *,
            name: Optional[str] = None,
            logger: Logger = None
        ):
        super().__init__(name=name, logger=logger)
        self._server: Optional[asyncio.AbstractServer] = None
        self.__serving_event = asyncio.Event()
        self.handlers: Dict[bytes, Callable] = {}
        self.sessions: Set[Session] = set()

    @property
    def is_serving(self) -> bool:
        return bool(self._server) and self._server.is_serving()
    
    def set_handlers(self, handlers: Dict[bytes, RequestHandler]):
        self.handlers.clear()
        self.handlers.update(handlers)
        return self
    
    def add_handlers(self, handlers: Dict[bytes, RequestHandler]):
        self.handlers.update(handlers)
        return self

    async def wait_serving(self) -> None:
        await self.__serving_event.wait()

    async def start_listening(self, onconnected: Callable) -> asyncio.AbstractServer:
        raise NotImplementedError()
    
    async def create_session(
            self,
            reader: asyncio.StreamReader,
            writer: asyncio.StreamWriter
    ) -> Session:
        """
        Creates a session for a new connection.

        This method must authenticate the connection when necessary
        and throw AuthenticationError on failure. If the client should
        be informed that the authentication has failed, it must be done
        in this method. It is not necessary to close the channel.
        """
        return SimpleServerSession(
            Channel(reader, writer),
            logger=self.logger,
            handlers=self.handlers
        )
    
    async def run(self):

        async def onconnected(
                reader: asyncio.StreamReader,
                writer: asyncio.StreamWriter
        ):
            try:
                self.logger.debug("received a connection")
                session = await self.create_session(reader, writer)
                self.logger.debug("accepted a connection")
                self.sessions.add(session)
                session.onclosed = self.sessions.remove
                self.emit(self.EV_CONNECTION, session)
                return # Prevent writer from being closed
            except ApplicationError as err:
                self.logger.error(err)
            except Exception as err:
                self.logger.exception(err)
            # This part of code is reached only on errors
            writer.close()
            await writer.wait_closed()

        def close_session(session: Session):
            session.onclosed = None
            return session.close()

        next_connect_try = 0
        loop = asyncio.get_running_loop()
        while True:
            try:
                # make a break in between consecutive connections
                current_time = loop.time()
                if current_time < next_connect_try:
                    await asyncio.sleep(next_connect_try - current_time)
                    next_connect_try += 1.0
                else:
                    next_connect_try = current_time + 1.0
                self._server = await self.start_listening(onconnected)
                self.__serving_event.set()
                await self._server.serve_forever()
            except asyncio.CancelledError:
                break
            except Exception as err:
                self.logger.exception(err)
            finally:
                self.__serving_event.clear()
        self._server = None
        await asyncio.gather(*map(close_session, self.sessions))
        self.sessions.clear()

    async def broadcast(self, cmd: bytes, data: Optional[bytes] = None):
        self.logger.debug(f"Broadcasting {cmd} to {len(self.sessions)} channels")
        await asyncio.gather(
            *(session.notify(cmd, data) for session in self.sessions),
            return_exceptions=True
        )


class UnixServer(AbstractServer):

    PRIVATE_MODE = S_IRWXU
    PUBLIC_MODE = S_IRWXU | S_IRWXG | S_IRWXO

    def __init__(self,
            path: str,
            mode: int = PRIVATE_MODE,
            *,
            name: Optional[str] = None,
            logger: Logger = None
    ):
        super().__init__(name=name, logger=logger)
        self.path = path
        self.mode = mode

    async def start_listening(self, onconnected: Callable):
        server = await asyncio.start_unix_server(onconnected, self.path)
        os.chmod(self.path, self.mode)
        return server
    
    async def unload(self):
        await super().unload()
        try:
            os.unlink(self.path)
        except Exception:
            pass


class TcpServer(AbstractServer):

    def __init__(self,
            host: str = 'localhost',
            port: Optional[int] = None,
            *,
            name: Optional[str] = None,
            logger: Logger = None
    ):
        super().__init__(name=name, logger=logger)
        self.host = host
        self.__port = port

    @property
    def port(self) -> int:
        return self.__port if self._server is None \
            else self._server.sockets[0].getsockname()[1]

    async def start_listening(self, onconnected: Callable) -> asyncio.AbstractServer:
        return await asyncio.start_server(onconnected, self.host, self.__port)
