import asyncio
from typing import Optional, List, Iterator, Set
from logging import Logger

from dataclassy import dataclass
from asyncinotify import Inotify, Mask,  Watch

from .base import Service
from video import loopback
from utils import logged_users


STREAMING_DELAY=0.1

def bool2str(value):
    return 'yes' if value else 'no'

class LoopbackDevice:
    __slots__ = (
        'index', 'owner', 'openers', 'streaming',
        'streaming_start_time', 'streaming_time',
        'watch', 'set_streaming_handle'
    )

    def __init__(self, index: int, owner: str):
        self.index = index
        self.owner = owner
        self.openers: int = 0
        self.streaming: bool = False
        self.streaming_start_time: float = 0.0
        self.streaming_time: float = 0.0
        self.watch: Optional[Watch] = None
        self.set_streaming_handle: Optional[asyncio.TimerHandle] = None

    @property
    def path(self) -> str:
        return f"/dev/video{self.index}"
    
    def mark_closed(self):
        self.openers = 0
        self.streaming = False
        self.watch = None
        if self.set_streaming_handle:
            self.set_streaming_handle.cancel()
            self.set_streaming_handle = None


@dataclass(slots=True)
class UpdateResult:
    new: int = 0
    orphaned: int = 0
    deleted: int = 0
    monitoring: int = 0
    ignored: int = 0
    removed: int = 0


class LoopbackService(Service):

    POLL_INTERVAL = 10*60

    EV_STREAM_ON = 'streamon'
    EV_STREAM_OFF = 'streamoff'

    def __init__(
        self,
        *,
        name: Optional[str] = None,
        logger: Optional[Logger] = None
    ):
        super().__init__(name=name, logger=logger)
        self.devices: List[LoopbackDevice] = []
        self.__lock: Optional[asyncio.Lock] = None
        self.__polling = False
        self.__modified: Set[str] = set()
        self.watcher = Inotify()


    def get_device_by_index(self, index: int) -> LoopbackDevice:
        return next(filter(lambda device: device.index == index, self.devices))

    def get_device_by_path(self, path: str) -> LoopbackDevice:
        return next(filter(lambda device: device.path == path, self.devices))
    
    def get_devices_by_owner(self, username: str) -> Iterator[LoopbackDevice]:
        return filter(lambda device: device.owner == username, self.devices)
    

    async def update(self) -> UpdateResult:

        report = UpdateResult()
        report.removed = len(self.devices)

        async with self.__lock:
            try:
                self.__polling = True
                self.logger.info(f'refreshing the list of loopback devices')

                # update the list of devices
                logged = logged_users()
                current = {
                    device.index : device for device in self.devices
                }
                self.devices.clear()

                for index, username in loopback.enumerate():
                    if username in logged:
                        device = current.get(index)
                        if device is None:
                            report.new += 1
                            self.logger.info(f'found /dev/video{index} owned by {username}')
                            device = LoopbackDevice(index, username)
                            device.watch = self.watcher.add_watch(device.path, Mask.OPEN | Mask.CLOSE)
                        self.devices.append(device)
                    else:
                        try:
                            report.orphaned += 1
                            self.logger.info(f'removing an orphaned device /dev/video{index} of {username}')
                            await loopback.delete(index)
                            report.deleted += 1
                        except Exception as err:
                            self.logger.warning(f'failed to remove /dev/video{index}: {err!r}')

                report.monitoring = len(self.devices)
                report.removed -= report.monitoring
                usage = await loopback.accessors()
                for device in self.devices:
                    if device.index in self.__modified:
                        self.logger.info(f'the status of {device.path} has changed - it will not be updated')
                        report.ignored += 1
                    else:
                        current_openers = len(usage.get(index, []))
                        if current_openers != device.openers:
                            device.openers = current_openers
                            if device.openers > 1: self._onopened(device)

            finally:
                self.__polling = False
                self.__modified.clear()

        self.logger.info(
            f'update report: new/orphaned/deleted: {report.new}/{report.orphaned}/{report.deleted}, '
            f'monitoring/removed/ignored: {report.monitoring}/{report.removed}/{report.ignored}'
        )
        return report


    async def load(self):
        self.__lock = asyncio.Lock()
        self.loop = asyncio.get_running_loop()
        await self.update()
        self.__monitor_task = asyncio.create_task(self.access_monitor())
        self.__polling_task = asyncio.create_task(self.access_polling(self.POLL_INTERVAL))

    async def unload(self):
        self.__monitor_task.cancel()
        self.__polling_task.cancel()
        await asyncio.gather(self.__monitor_task, self.__polling_task)
        for device in self.devices:
            try:
                await loopback.delete(device.index)
            except Exception as err:
                self.logger.warning(f"failed to delete {device.path}: {err!r}")
        self.devices.clear()


    async def access_polling(self, interval):
        while True:
            try:
                await self.update()
                await asyncio.sleep(interval)
            except asyncio.CancelledError:
                break
            except Exception:
                # errors are already reported by update()
                pass


    def _set_streaming(self, device: LoopbackDevice):
        if device.openers < 2: return  # a safe check
        device.set_streaming_handle = None
        device.streaming = True
        device.streaming_start_time = self.loop.time()
        self.logger.info(f'device {device.path} has been opened for streaming')
        self.emit(self.EV_STREAM_ON, device)

    def _onopened(self, device: LoopbackDevice):
        device.openers += 1
        self.logger.debug('{} has been opened. Current openers: {}. Streaming: {}'.format(device.path, device.openers, bool2str(device.streaming)))
        # delay announcing streaming state if first time opened
        if not device.streaming \
            and device.set_streaming_handle is None \
            and device.openers > 1:
            self.logger.debug(f'{device.path} might be opened streaming - scheduling a status update in {STREAMING_DELAY} seconds')
            device.set_streaming_handle = self.loop.call_later(
                STREAMING_DELAY, self._set_streaming, device
            )

    def _onclosed(self, device: LoopbackDevice):
        device.openers = max(device.openers-1, 0)
        self.logger.debug('{} has been closed. Current openers: {}. Streaming: {}'.format(device.path, device.openers, bool2str(device.streaming)))
        if device.openers > 1: # still opened for streaming
            pass
        elif device.streaming:
            device.streaming = False
            device.streaming_time += self.loop.time() - device.streaming_start_time
            self.logger.info(f'streaming from {device.path} has stopped')
            self.emit(self.EV_STREAM_OFF, device)
        elif device.set_streaming_handle:
            self.logger.debug(f'cancelling the status update for {device.path}')
            device.set_streaming_handle.cancel()
            device.set_streaming_handle = None


    async def access_monitor(self):

        event_type = {
            Mask.OPEN: "open",
            Mask.CLOSE_NOWRITE: "close",
            Mask.CLOSE_WRITE: "close"
        }

        while True:
            try:
                event = await self.watcher.get()
                self.logger.debug(f'got an event {event_type.get(event.mask, event.mask)} for {str(event.path)}')
                device = self.get_device_by_path(str(event.path))
                if event.mask == Mask.OPEN:
                    self._onopened(device)
                elif event.mask & Mask.CLOSE:
                    self._onclosed(device)
                else:
                    continue
                if self.__polling:
                    self.__modified.add(device.path)
            except asyncio.CancelledError:
                break
            except StopIteration:
                self.logger.debug(f'inotify iterator has stopped - restarting')
                # This happens when a loopback device is removed
                pass
            except Exception as err:
                self.logger.error(repr(err))

        # remove watchers and clear notifiers
        self.watcher.close()
        self.watcher = None
        for device in self.devices:
            device.mark_closed()


    async def get_device(self, username: str) -> Optional[LoopbackDevice]:
        async with self.__lock:
            device = next(self.get_devices_by_owner(username), None)
            if device is None:
                self.logger.debug(f'creating a device for {username}')
                device = LoopbackDevice(await loopback.create(username), username)
                self.devices.append(device)
                # Wait for system to process the new device
                await asyncio.sleep(0.1)
                device.watch = self.watcher.add_watch(device.path, Mask.OPEN | Mask.CLOSE)
                self.logger.debug(f'created {device.path} for {username}')
        return device
    
    async def remove_device(self, username: str) -> Optional[LoopbackDevice]:
        async with self.__lock:
            device: Optional[LoopbackDevice] = next(self.get_devices_by_owner(username), None)
            if device is None: return None
            self.devices.remove(device)
            self.logger.debug(f'stops monitoring {device.path} owned by {username}')
            self.watcher.rm_watch(device.watch)
            self.logger.debug(f'deleting {device.path} owned by {username}')
            await loopback.delete(device.index)
            self.logger.debug(f'removed {device.path} owned by {username}')
            return device
