import asyncio
from logging import Logger
from typing import List, Callable, Optional

from pyudev import Monitor, MonitorObserver, \
    Context as UdevContext, Device as UdevDevice
from v4l2py.device import Device as V4L2Device

from video.v4l2utils import CameraInfo
from service.base import Service


class CameraCollection(List[CameraInfo]):
    """
    A collection of CameraInfo objects that can be retrieved by `id` or `path`
    properties.
    """

    def find(self, predicate: Callable[[CameraInfo], bool]) -> Optional[CameraInfo]:
        return next(filter(predicate, self), None)
    
    def extract(self, predicate: Callable[[CameraInfo], bool]) -> Optional[CameraInfo]:
        found = next(filter(lambda item: predicate(item[1]), enumerate(self)), None)
        return self.pop(found[0]) if found else None

    def default(self, width: int = 0, height: int = 0) -> Optional[CameraInfo]:
        return self.find(lambda cam: cam.supports(width, height)) or next(self, None)
    
    def best_match(self, id: str, width: int, height: int) -> Optional[CameraInfo]:
        return self.get_id(id) or self.default(width, height)
    
    def get_id(self, id: str) -> Optional[CameraInfo]:
        # id is either the index of the id of a camera
        if len(id) < 3:
            try:
                index = int(id)
                if index > 0 and index <= len(self):
                    return self[index-1]
            except Exception:
                pass
            return None
        else:
            return self.find(lambda c: c.id == id) or self.find(lambda c: c.is_match(id))
    
    def get_path(self, path: str) -> Optional[CameraInfo]:
        return self.find(lambda c: c.path == path)
    
    def remove_id(self, id: str) -> Optional[CameraInfo]:
        return self.extract(lambda c: c.id == id)

    def remove_path(self, path: str) -> Optional[CameraInfo]:
        return self.extract(lambda c: c.path == path)



class CameraEventHandler(Callable[[V4L2Device], None]):
    """
    A signature for handlers of `EV_CAMERA_ATTACHED` and `EV_CAMERA_DETACHED` events
    triggered by `VideDevicesMonitor`.
    """


class VideoDevicesMonitor(Service):
    """
    A monitor service for video input devices.
    Provides a list of currently available cameras that it
    updates whenever a camera is plugged or unplugged.
    
    Events
    ------
    - `EV_CAMERA_ATTACHED(CameraInfo)` - a camera has been attached
    - `EV_CAMERA_DETACHED(CameraInfo)` - a camera is removed

    Public methods and properties
    -------
    `cameras: CameraCollection`
        A list of available video input devices

    `refresh() -> None`
        Updates the current list of cameras
        
    `async process_udev_event(device: pyudev.Device) -> None`
        Processes an event emitted by `pyudev.Monitor`: updates `cameres`
        and emits `EV_CAMERA_ATTACHED` or `EV_CAMERA_DETACHED` when necessary.
    """

    EV_CAMERA_ATTACHED = 'attach'
    EV_CAMERA_DETACHED = 'detach'
    EV_UPDATE = 'update'

    def __init__(
            self, *,
            name: Optional[str] = None,
            logger: Optional[Logger] = None
    ):
        super().__init__(name=name, logger=logger)
        self.cameras = CameraCollection()


    @property
    def running(self) -> bool:
        """Return the current state of the service"""
        return self.__udev is not None


    async def start(self):
        """Loads the current list of cameras and starts monitoring video devices"""

        def handle_action(device):
            asyncio.run_coroutine_threadsafe(
                self.process_udev_event(device),
                loop
            )

        loop = asyncio.get_event_loop()

        self.logger.debug('creating a device monitor')
        self.__udev = UdevContext()        
        monitor = Monitor.from_netlink(self.__udev)
        monitor.filter_by('video4linux')
        self.__observer = MonitorObserver(monitor, callback=handle_action)
        self.__observer.start()
        self.logger.debug('monitoring devices')

        self.__load_cameras()


    async def stop(self):
        """Clears the list of cameras and stops monitoring video devices"""

        self.__observer.stop()
        self.cameras.clear()
        del self.__observer
        del self.__udev


    def __load_cameras(self):
        """Appends `CameraInfo` object to the list of cameras per video input device"""
        for device in self.__udev.list_devices(subsystem='video4linux'):
            try:
                camera = CameraInfo.from_path(device.device_node)
                if camera:
                    self.logger.info(f'found {camera.path}')
                    self.cameras.append(camera)
            except Exception as err:
                self.logger.exception(err)
        self.cameras.sort(key = lambda info: info.path)


    def refresh(self):
        """
        Refreshes the list `cameras` of video input devices to match
        the current situation. Old entries are removed, but references
        to the list are preserved.
        """

        self.logger.debug('refreshing a list of devices')
        if not self.running:
            raise ValueError("this method can be called only on a running service")
        self.cameras.clear()
        self.__load_cameras()
        self.emit(self.EV_UPDATE, self.cameras)


    async def process_udev_event(self, event: UdevDevice):
        """
        Processes an event received from a udev Monitor: updates the list of cameras
        and emits `EV_CAMERA_ATTACHED` or `EV_CAMERA_DETACHED` when necessary.
        """
        
        try:
            self.logger.debug(f'udev:{event.action} {event.device_node}')
            if event.action == 'add':
                device = CameraInfo.from_path(event.device_node)
                if device:
                    self.logger.info(f'adding {device.path}')
                    self.cameras.insert(0, device)
                    self.emit(self.EV_CAMERA_ATTACHED, device)
            elif event.action == 'remove':
                device = self.cameras.remove_path(event.device_node)
                if device:
                    self.logger.info(f'removing {device.path}')
                    self.emit(self.EV_CAMERA_DETACHED, device)
        except Exception as err:
            self.logger.exception(err)
