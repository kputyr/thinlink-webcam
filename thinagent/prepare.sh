#!/bin/sh

# This script prepares files for a ThinAgent.

TARGET_DIR=tlwebcam

SOURCE_DIR="../src"
OPT_DIR="../opt"

[ -d $SOURCE_DIR ] && [ -d $OPT_DIR ] || \
 { echo "Cannot find source files. Please check if the script is started in the correct directory."; exit 1; }

echo "Copying Python files..."
[ -d tlwebcam ] && \rm -rf tlwebcam
for FILE in `cat files`; do
    DIR=`dirname $TARGET_DIR/$FILE`
    mkdir -p $DIR
    cp $SOURCE_DIR/$FILE $DIR
done

echo "Choose the constants file for ThinAgent..."
mkdir -p $TARGET_DIR/constants
cp "$SOURCE_DIR/constants/thinagent.py" "$TARGET_DIR/constants/__init__.py"

echo "Copy service files..."
cp $OPT_DIR/tlwebcam.service $TARGET_DIR

echo "Copy ThinLinc hook files..."
mkdir -p $TARGET_DIR/init.d
cp $OPT_DIR/tlwebcam-load.sh $OPT_DIR/tlwebcam-unload.sh $OPT_DIR/tlwsrvc.sh $TARGET_DIR/init.d

echo "Creating an archive file..."
tar -zcvf `date +"tlwebcam-%Y%m%d.tar.gz"` tlwebcam install.sh dependencies

echo "Done"

