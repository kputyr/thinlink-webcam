#!/bin/sh

# The installation script for tlwebcam on a ThinAgent

CWD=$PWD
SOURCE_DIR=`basename $PWD`/tlwebcam
TARGET_DIR=/opt/tlwebcam
BIN_DIR=/usr/bin
SYSTEM_DIR=/etc/systemd/system
THINLINC_DIR=/opt/thinlinc

# Check if python, pip, and v4l2loopback-ctl are available
for PROG in python pip v4l2loopback-ctl; do
  which $PROG > /dev/null || \
  { echo "Missing $PROG - please check your OS"; exit 1; }
done

echo "Stopping the service..."
systemctl stop tlwebcam

# Install python libraries
echo "Installing required python libraries..."
for MODULE in `cat dependencies`; do
    echo "Installing $MODULE..."
    pip install $PYTHON3_LIB $MODULE
done

# Copy files and create shortcuts
echo "Installing tlwebcam..."
[ -d $TARGET_DIR ] && \rm -rf $TARGET_DIR/* || mkdir $TARGET_DIR
cp -r tlwebcam/* $TARGET_DIR

cd $SYSTEM_DIR
ln -snf $TARGET_DIR/tlwebcam.service tlwebcam.service

cd $BIN_DIR
ln -snf $TARGET_DIR/tlwebcam-ctl.py tlwebcam-ctl

echo "Copying ThinLinc hook scripts..."
cd $THINLINC_DIR/etc/xstartup.d
ln -snf $TARGET_DIR/init.d/tlwebcam-load.sh 91-tlwebcam-load

cd ../xlogout.d
ln -snf $TARGET_DIR/init.d/tlwebcam-unload.sh 91-tlwebcam-unload

cd $CWD

echo "Starting the service..."
systemctl daemon-reaload
systemctl start tlwebcam

# That's it!
echo "Ready!"
