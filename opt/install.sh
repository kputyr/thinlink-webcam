#!/bin/sh

TLPREFIX="/opt/thinlinc"

echo "Stopping the service tlwebcam if running"
service tlwebcam stop

echo "Preparing the target directory"
mkdir -p /opt/tlwebcam
\rm -rf /opt/tlwebcam/*

echo "Copying files"
cp -r ../src/* /opt/tlwebcam/
\rm -f /opt/tlwebcam/constants/__init__.py
mv /opt/tlwebcam/constants/tl.py /opt/tlwebcam/constants/__init__.py

echo "Installing the service"
cp ./tlwsrvc.sh /opt/tlwebcam/
cp ./tlwebcam.service /etc/systemd/system/

echo "Installing tl hooks"
cp ./tlwebcam-load.sh "$TLPREFIX/etc/xstartup.d/"
cp ./tlwebcam-unload.sh "$TLPREFIX/etc/xlogout.d/"

echo "Done. Make sure to update v4l2loopback files before running the service."

