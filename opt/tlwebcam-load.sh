#!/bin/bash

# ThinLinc Webcam
# author: Krzysztof Putyra
# date: 09/03/2023
#
# This script makes a request to the tlwebcam daemon to create
# a video loopback device for the current user. It should be
# located in /opt/thinlinc/etc/xstartup.d

LOG_FILE="/var/log/tlwebcam.log"

USERNAME=`id -un`
SLOTS=`tl-config /vsm/tunnelslots_per_session`
SERVICE_SLOT=`tl-config /vsm/tunnelservices/tlwebcam`

if [ -z "$SERVICE_SLOT" ]; then
  echo "[tlwebcam] WARNING no tunneling configured!"
  SERVICE_SLOT=9
fi

TLDISPLAY=`tl-session-param display`

echo "[tlwebcam] $USERNAME INFO initializing the service. Session display $TLDISPLAY"

if [[ $TLDISPLAY -lt 100 ]]; then
  # Formula for tunnel port number for session with diplay < 100 from
  # https://www.cendio.com/resources/docs/tag/tcp-ports_agent.html
  BASE=`tl-config /vsm/tunnel_bind_base`
  PORT=$(($BASE + $TLDISPLAY * $SLOTS + $SERVICE_SLOT))
else
  # Formula for tunnel port number for session with display >= 100
  # https://www.cendio.com/resources/docs/tag/tcp-ports_agent.html
  BASE=`tl-config /vsmagent/max_session_port`
  PORT=$(($BASE - ($TLDISPLAY - 100) * ($SLOTS + 1) - 9 + $SERVICE_SLOT))
fi

echo "[tlwebcam] $USERNAME INFO Starting listening on port $PORT"
python /opt/tlwebcam/tlwebcam.py $PORT >> $LOG_FILE 2>&1 &

