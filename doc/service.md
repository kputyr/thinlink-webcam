# Services

## Starting a service

A service is started by running the coroutine `service.start()`.
This coroutine logs loading stages, loads service-related data
by running a coroutine `service.load()` that should be implemented
in derived classes, and emits `start` event on success.

## Stopping a service

To stop a service run the coroutine `service.stop()`. It logs unloading
stages, unload service-related data by running `service.unload()` that
should be implemented in derived classes, and emist `stop` event o
success.
